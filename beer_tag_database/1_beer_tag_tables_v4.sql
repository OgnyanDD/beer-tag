-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema beer_tag_database
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema beer_tag_database
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `beer_tag_database` DEFAULT CHARACTER SET latin1 ;
USE `beer_tag_database` ;

-- -----------------------------------------------------
-- Table `beer_tag_database`.`styles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`styles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` NVARCHAR(45) NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'This is a Styles table in \"Beer Tag\" Web Application';


-- -----------------------------------------------------
-- Table `beer_tag_database`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`countries` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'This is a Origin Countries table in \"Beer Tag\" Web Application';


-- -----------------------------------------------------
-- Table `beer_tag_database`.`breweries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`breweries` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `brewery` VARCHAR(45) NOT NULL,
  `country_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_breweries_countries`
    FOREIGN KEY (`country_id`)
    REFERENCES `beer_tag_database`.`countries` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'This is a Breweries table in \"Beer Tag\" Web Application';

CREATE INDEX `fk_breweries_countries_idx` ON `beer_tag_database`.`breweries` (`country_id` ASC);

CREATE UNIQUE INDEX `brewery_UNIQUE` ON `beer_tag_database`.`breweries` (`brewery` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_database`.`beers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`beers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `picture` LONGBLOB NOT NULL,
  `label` VARCHAR(45) NOT NULL,
  `style_id` INT(11) NOT NULL,
  `abv` DOUBLE NOT NULL,
  `brewery_id` INT NOT NULL,
  `description` TEXT NOT NULL,
  `is_deleted` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_beers_styles`
    FOREIGN KEY (`style_id`)
    REFERENCES `beer_tag_database`.`styles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_beers_breweries`
    FOREIGN KEY (`brewery_id`)
    REFERENCES `beer_tag_database`.`breweries` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COMMENT = 'This is a Beers table in \"Beer Tag\" Web Application';

CREATE INDEX `fk_beers_styles_idx` ON `beer_tag_database`.`beers` (`style_id` ASC);

CREATE INDEX `fk_beers_breweries_idx` ON `beer_tag_database`.`beers` (`brewery_id` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_database`.`login`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`login` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(32) NOT NULL,
  `user_password` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'This is LogIn table in \"Beer Tag\" Web Application';

CREATE UNIQUE INDEX `login_user_name_UNIQUE` ON `beer_tag_database`.`login` (`user_name` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_database`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `profile_picture` LONGBLOB NOT NULL,
  `first_name` VARCHAR(15) NOT NULL,
  `last_name` VARCHAR(20) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `login_id` INT NOT NULL COMMENT 'This is a Users table from \"Beer Tag\" Web Application',
  `is_deleted` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_users_login`
    FOREIGN KEY (`login_id`)
    REFERENCES `beer_tag_database`.`login` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'This is a Users table in \"Beer Tag\" Web Application';

CREATE INDEX `fk_users_login_idx` ON `beer_tag_database`.`users` (`login_id` ASC);

CREATE UNIQUE INDEX `user_email_UNIQUE` ON `beer_tag_database`.`users` (`email` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_database`.`status_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`status_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'This is a Beers Types table in \"Beer Tag\" Web Application';


-- -----------------------------------------------------
-- Table `beer_tag_database`.`beers_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`beers_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `beer_id` INT NOT NULL,
  `status_type_id` INT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_beers_status_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `beer_tag_database`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beers_status_beers`
    FOREIGN KEY (`beer_id`)
    REFERENCES `beer_tag_database`.`beers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beers_status_status_types`
    FOREIGN KEY (`status_type_id`)
    REFERENCES `beer_tag_database`.`status_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'This is a Beers Status table in \"Beer Tag\" Web Application';

CREATE INDEX `fk_beers_status_users_idx` ON `beer_tag_database`.`beers_status` (`user_id` ASC);

CREATE INDEX `fk_beers_status_beers_idx` ON `beer_tag_database`.`beers_status` (`beer_id` ASC);

CREATE INDEX `fk_beers_status_status_types_idx` ON `beer_tag_database`.`beers_status` (`status_type_id` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_database`.`beers_ratings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`beers_ratings` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `beer_id` INT NOT NULL,
  `rating` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_beers_ratings_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `beer_tag_database`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beers_ratings_beers`
    FOREIGN KEY (`beer_id`)
    REFERENCES `beer_tag_database`.`beers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'This is a Beers Ratings table in \"Beer Tag\" Web Application';

CREATE INDEX `fk_beers_ratings_users_idx` ON `beer_tag_database`.`beers_ratings` (`user_id` ASC);

CREATE INDEX `fk_beers_ratings_beers_idx` ON `beer_tag_database`.`beers_ratings` (`beer_id` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_database`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`tags` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tag` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'This is a Tags table in \"Beer Tag\" Web Application';

CREATE UNIQUE INDEX `tag_UNIQUE` ON `beer_tag_database`.`tags` (`tag` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_database`.`beers_tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`beers_tags` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `beer_id` INT NOT NULL,
  `tag_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_beers_tags_beers`
    FOREIGN KEY (`beer_id`)
    REFERENCES `beer_tag_database`.`beers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_beers_tags_tags`
    FOREIGN KEY (`tag_id`)
    REFERENCES `beer_tag_database`.`tags` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'This is a Beers Tags relational table in \"Beer Tag\" Web Application';

CREATE INDEX `fk_beers_tags_beers_idx` ON `beer_tag_database`.`beers_tags` (`beer_id` ASC);

CREATE INDEX `fk_beers_tags_tags_idx` ON `beer_tag_database`.`beers_tags` (`tag_id` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_database`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'This is a Roles table from \"Beer Tag\" Web Application';


-- -----------------------------------------------------
-- Table `beer_tag_database`.`login_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beer_tag_database`.`login_roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login_id` INT NOT NULL,
  `role_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_users_roles_login`
    FOREIGN KEY (`login_id`)
    REFERENCES `beer_tag_database`.`login` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_roles_roles`
    FOREIGN KEY (`role_id`)
    REFERENCES `beer_tag_database`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'This is a Login Roles relation table from \"Beer Tag\" Web Application';

CREATE INDEX `fk_users_roles_login_idx` ON `beer_tag_database`.`login_roles` (`login_id` ASC);

CREATE INDEX `fk_users_roles_roles_idx` ON `beer_tag_database`.`login_roles` (`role_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
