use beer_tag_database;

-- -------------------------------------- <USER> -------------------------------------- -- 

#
# Dumping data for table 'login'
#

INSERT INTO login
VALUES
(NULL, 'OgyDD', 'Password01'),
(NULL, 'PavelK', 'Password02'),
(NULL, 'Simona99', 'Password03');

#
# Dumping data for table 'roles'
#

INSERT INTO roles (id, role)
VALUES 
(NULL, 'user'),
(NULL, 'administrator');

#
# Dumping data for table 'users'
#

INSERT INTO users
VALUES
(NULL, 'user_profile.png', 'Ognyan', 'Dimitrov', 'ognyan_dimitrov@beer.tg', 1, 0),
(NULL, 'user_profile.png', 'Pavel', 'Karaivanov', 'pavel_karaivanov@beer.tg', 2, 0),
(NULL, 'user_profile.png', 'Simona', 'Petrova', 'simona1990@ql.sf', 3, 0);

#
# Dumping data for table 'login_roles'
#

INSERT INTO login_roles
VALUES
(NULL, 1, 2),
(NULL, 2, 2),
(NULL, 3, 1);

-- -------------------------------------- <BEER> -------------------------------------- -- 

#
# Dumping data for table 'origin_countries'
#

INSERT INTO countries (id, country)
VALUES 
(NULL, 'Afghanistan'),
(NULL, 'Albania'),
(NULL, 'Algeria'),
(NULL, 'Andorra'),
(NULL, 'Angola'),
(NULL, 'Antigua and Barbuda'),
(NULL, 'Argentina'),
(NULL, 'Armenia'),
(NULL, 'Australia'),
(NULL, 'Austria'),
(NULL, 'Azerbaijan'),
(NULL, 'Bahrain'),
(NULL, 'Bangladesh'),
(NULL, 'Barbados'),
(NULL, 'Belarus'),
(NULL, 'Belgium'),
(NULL, 'Belize'),
(NULL, 'Benin'),
(NULL, 'Bhutan'),
(NULL, 'Bolivia'),
(NULL, 'Bosnia and Herzegovina'),
(NULL, 'Botswana'),
(NULL, 'Brazil'),
(NULL, 'Brunei'),
(NULL, 'Bulgaria'),
(NULL, 'Burkina Faso'),
(NULL, 'Burundi'),
(NULL, 'Cabo Verde'),
(NULL, 'Cambodia'),
(NULL, 'Cameroon'),
(NULL, 'Canada'),
(NULL, 'Central African Republic'),
(NULL, 'Chad'),
(NULL, 'Chile'),
(NULL, 'China'),
(NULL, 'Colombia'),
(NULL, 'Comoros'),
(NULL, 'Congo'),
(NULL, 'Costa Rica'),
(NULL, 'Côte d’Ivoire'),
(NULL, 'Croatia'),
(NULL, 'Cuba'),
(NULL, 'Cyprus'),
(NULL, 'Czech Republic'),
(NULL, 'Denmark'),
(NULL, 'Djibouti'),
(NULL, 'Dominica'),
(NULL, 'Dominican Republic'),
(NULL, 'East Timor (Timor-Leste)'),
(NULL, 'Ecuador'),
(NULL, 'Egypt'),
(NULL, 'El Salvador'),
(NULL, 'Equatorial Guinea'),
(NULL, 'Eritrea'),
(NULL, 'Estonia'),
(NULL, 'Eswatini'),
(NULL, 'Ethiopia'),
(NULL, 'Fiji'),
(NULL, 'Finland'),
(NULL, 'France'),
(NULL, 'Gabon'),
(NULL, 'Georgia'),
(NULL, 'Germany'),
(NULL, 'Ghana'),
(NULL, 'Greece'),
(NULL, 'Grenada'),
(NULL, 'Guatemala'),
(NULL, 'Guinea'),
(NULL, 'Guinea-Bissau'),
(NULL, 'Guyana'),
(NULL, 'Haiti'),
(NULL, 'Honduras'),
(NULL, 'Hungary'),
(NULL, 'Iceland'),
(NULL, 'India'),
(NULL, 'Indonesia'),
(NULL, 'Iran'),
(NULL, 'Iraq'),
(NULL, 'Ireland'),
(NULL, 'Israel'),
(NULL, 'Italy'),
(NULL, 'Jamaica'),
(NULL, 'Japan'),
(NULL, 'Jordan'),
(NULL, 'Kazakhstan'),
(NULL, 'Kenya'),
(NULL, 'Kiribati'),
(NULL, 'Kosovo'),
(NULL, 'Kuwait'),
(NULL, 'Kyrgyzstan'),
(NULL, 'Laos'),
(NULL, 'Latvia'),
(NULL, 'Lebanon'),
(NULL, 'Lesotho'),
(NULL, 'Liberia'),
(NULL, 'Libya'),
(NULL, 'Liechtenstein'),
(NULL, 'Lithuania'),
(NULL, 'Luxembourg'),
(NULL, 'Madagascar'),
(NULL, 'Malawi'),
(NULL, 'Malaysia'),
(NULL, 'Maldives'),
(NULL, 'Mali'),
(NULL, 'Malta'),
(NULL, 'Marshall Islands'),
(NULL, 'Mauritania'),
(NULL, 'Mauritius'),
(NULL, 'Mexico'),
(NULL, 'Micronesia'),
(NULL, 'Moldova'),
(NULL, 'Monaco'),
(NULL, 'Mongolia'),
(NULL, 'Montenegro'),
(NULL, 'Morocco'),
(NULL, 'Mozambique'),
(NULL, 'Myanmar (Burma)'),
(NULL, 'Namibia'),
(NULL, 'Nauru'),
(NULL, 'Nepal'),
(NULL, 'Netherlands'),
(NULL, 'New Zealand'),
(NULL, 'Nicaragua'),
(NULL, 'Niger'),
(NULL, 'Nigeria'),
(NULL, 'North Korea'),
(NULL, 'North Macedonia'),
(NULL, 'Norway'),
(NULL, 'Oman'),
(NULL, 'Pakistan'),
(NULL, 'Palau'),
(NULL, 'Panama'),
(NULL, 'Papua New Guinea'),
(NULL, 'Paraguay'),
(NULL, 'Peru'),
(NULL, 'Philippines'),
(NULL, 'Poland'),
(NULL, 'Portugal'),
(NULL, 'Qatar'),
(NULL, 'Romania'),
(NULL, 'Russia'),
(NULL, 'Rwanda'),
(NULL, 'Saint Kitts and Nevis'),
(NULL, 'Saint Lucia'),
(NULL, 'Saint Vincent and the Grenadines'),
(NULL, 'Samoa'),
(NULL, 'San Marino'),
(NULL, 'Sao Tome and Principe'),
(NULL, 'Saudi Arabia'),
(NULL, 'Senegal'),
(NULL, 'Serbia'),
(NULL, 'Seychelles'),
(NULL, 'Sierra Leone'),
(NULL, 'Singapore'),
(NULL, 'Slovakia'),
(NULL, 'Slovenia'),
(NULL, 'Solomon Islands'),
(NULL, 'Somalia'),
(NULL, 'South Africa'),
(NULL, 'South Korea'),
(NULL, 'South Sudan'),
(NULL, 'Spain'),
(NULL, 'Sri Lanka'),
(NULL, 'Sudan'),
(NULL, 'Suriname'),
(NULL, 'Sweden'),
(NULL, 'Switzerland'),
(NULL, 'Syria'),
(NULL, 'Taiwan'),
(NULL, 'Tajikistan'),
(NULL, 'Tanzania'),
(NULL, 'Thailand'),
(NULL, 'The Bahamas'),
(NULL, 'The Gambia'),
(NULL, 'Togo'),
(NULL, 'Tonga'),
(NULL, 'Trinidad and Tobago'),
(NULL, 'Tunisia'),
(NULL, 'Turkey'),
(NULL, 'Turkmenistan'),
(NULL, 'Tuvalu'),
(NULL, 'Uganda'),
(NULL, 'Ukraine'),
(NULL, 'United Arab Emirates'),
(NULL, 'United Kingdom'),
(NULL, 'United States'),
(NULL, 'Uruguay'),
(NULL, 'Uzbekistan'),
(NULL, 'Vanuatu'),
(NULL, 'Vatican City'),
(NULL, 'Venezuela'),
(NULL, 'Vietnam'),
(NULL, 'Yemen'),
(NULL, 'Zambia'),
(NULL, 'Zimbabwe');

#
# Dumping data for table 'breweries'
#

INSERT INTO breweries (id, brewery, country_id)
VALUES 
(NULL, 'Ailyak', 25),
(NULL, 'Beer Bastards', 25),
(NULL, 'Beerbox', 25),
(NULL, 'Birariya River Side', 25),
(NULL, 'Boliarka-VT Brewery', 25),
(NULL, 'Brothers Brew Team', 25),
(NULL, 'Brunch Family', 25),
(NULL, 'Hills Brewery', 25),
(NULL, 'Lomsko Pivo AD', 25),
(NULL, 'Metalhead Brewery', 25),
(NULL, 'Pirinsko Pivo OOD', 25),
(NULL, 'Pivovarna Zagorka', 25);

#
# Dumping data for table 'styles'
#

INSERT INTO styles (id, type, category)
VALUES
(NULL, 'Altbier', 'Anglo-American Ales'),
(NULL, 'Amber Ale', 'Anglo-American Ales'),
(NULL, 'American Strong Ale', 'Anglo-American Ales'),
(NULL, 'Barley Wine/Wheat Wine', 'Anglo-American Ales'),
(NULL, 'Bitter', 'Anglo-American Ales'),
(NULL, 'Brown Ale', 'Anglo-American Ales'),
(NULL, 'Cream Ale', 'Anglo-American Ales'),
(NULL, 'English Strong Ale', 'Anglo-American Ales'),
(NULL, 'Golden Ale/Blond Ale', 'Anglo-American Ales'),
(NULL, 'IPA', 'Anglo-American Ales'),
(NULL, 'IPA-Imperial/Double', 'Anglo-American Ales'),
(NULL, 'IPA-Session', 'Anglo-American Ales'),
(NULL, 'Irish Ale', 'Anglo-American Ales'),
(NULL, 'Kölsch', 'Anglo-American Ales'),
(NULL, 'Mild Ale', 'Anglo-American Ales'),
(NULL, 'Old Ale', 'Anglo-American Ales'),
(NULL, 'Pale Ale-American', 'Anglo-American Ales'),
(NULL, 'Pale Ale-English', 'Anglo-American Ales'),
(NULL, 'Premium Bitter/ESB', 'Anglo-American Ales'),
(NULL, 'Scotch Ale/Wee Heavy', 'Anglo-American Ales'),
(NULL, 'Scottish Ale', 'Anglo-American Ales'),
(NULL, 'Abbey Dubbel', 'Belgian-Style Ales'),
(NULL, 'Abbey Quadrupel/Abt', 'Belgian-Style Ales'),
(NULL, 'Abbey Tripel', 'Belgian-Style Ales'),
(NULL, 'Belgian Ale', 'Belgian-Style Ales'),
(NULL, 'Belgian Strong Ale', 'Belgian-Style Ales'),
(NULL, 'Bière de Garde', 'Belgian-Style Ales'),
(NULL, 'Saison', 'Belgian-Style Ales'),
(NULL, 'Cider', 'Cider, Mead, Sake'),
(NULL, 'Ice Cider/Ice Perry', 'Cider, Mead, Sake'),
(NULL, 'Mead', 'Cider, Mead, Sake'),
(NULL, 'Perry', 'Cider, Mead, Sake'),
(NULL, 'Saké-Daiginjo', 'Cider, Mead, Sake'),
(NULL, 'Saké-Futsu-shu', 'Cider, Mead, Sake'),
(NULL, 'Saké-Genshu', 'Cider, Mead, Sake'),
(NULL, 'Saké-Ginjo', 'Cider, Mead, Sake'),
(NULL, 'Saké-Honjozo', 'Cider, Mead, Sake'),
(NULL, 'Saké-Infused', 'Cider, Mead, Sake'),
(NULL, 'Saké-Junmai', 'Cider, Mead, Sake'),
(NULL, 'Saké-Koshu', 'Cider, Mead, Sake'),
(NULL, 'Saké-Namasaké', 'Cider, Mead, Sake'),
(NULL, 'Saké-Nigori', 'Cider, Mead, Sake'),
(NULL, 'Saké-Taru', 'Cider, Mead, Sake'),
(NULL, 'Saké-Tokubetsu', 'Cider, Mead, Sake'),
(NULL, 'Amber Lager/Vienna', 'Lagers'),
(NULL, 'Bock-Doppelbock', 'Lagers'),
(NULL, 'Bock-Dunkler/Urbock', 'Lagers'),
(NULL, 'Bock-Eisbock', 'Lagers'),
(NULL, 'Bock-Heller/Maibock', 'Lagers'),
(NULL, 'California Common', 'Lagers'),
(NULL, 'Czech Pilsner (Světlý)', 'Lagers'),
(NULL, 'Dortmunder', 'Lagers'),
(NULL, 'Dunkel/Tmavy', 'Lagers'),
(NULL, 'Helles', 'Lagers'),
(NULL, 'Imperial Pils/Strong Pale Lager', 'Lagers'),
(NULL, 'India Style Lager', 'Lagers'),
(NULL, 'Malt Liquor', 'Lagers'),
(NULL, 'Oktoberfest/Märzen', 'Lagers'),
(NULL, 'Pale Lager', 'Lagers'),
(NULL, 'Pilsener', 'Lagers'),
(NULL, 'Polotmavý', 'Lagers'),
(NULL, 'Premium Lager', 'Lagers'),
(NULL, 'Radler/Shandy', 'Lagers'),
(NULL, 'Schwarzbier', 'Lagers'),
(NULL, 'Zwickel/Keller/Landbier', 'Lagers'),
(NULL, 'Berliner Weisse', 'Sour Beer'),
(NULL, 'Lambic-Faro', 'Sour Beer'),
(NULL, 'Lambic-Fruited', 'Sour Beer'),
(NULL, 'Lambic-Gueuze', 'Sour Beer'),
(NULL, 'Lambic-Unblended', 'Sour Beer'),
(NULL, 'Sour/Wild Ale', 'Sour Beer'),
(NULL, 'Sour Red/Brown', 'Sour Beer'),
(NULL, 'IPA-Black/Dark', 'Stout and Porter'),
(NULL, 'Porter', 'Stout and Porter'),
(NULL, 'Porter-Baltic', 'Stout and Porter'),
(NULL, 'Porter-Imperial/Double', 'Stout and Porter'),
(NULL, 'Stout', 'Stout and Porter'),
(NULL, 'Stout-Dry', 'Stout and Porter'),
(NULL, 'Stout-Foreign/Extra', 'Stout and Porter'),
(NULL, 'Stout-Imperial', 'Stout and Porter'),
(NULL, 'Stout-Sweet', 'Stout and Porter'),
(NULL, 'Bock-Weizenbock', 'Wheat Beer'),
(NULL, 'Grodziskie/Gose/Lichtenhainer', 'Wheat Beer'),
(NULL, 'Weissbier-Dunkelweizen', 'Wheat Beer'),
(NULL, 'Weissbier-Hefeweizen', 'Wheat Beer'),
(NULL, 'Weissbier-Kristallweizen', 'Wheat Beer'),
(NULL, 'Wheat Ale', 'Wheat Beer'),
(NULL, 'Witbier', 'Wheat Beer'),
(NULL, 'Fruit Beer', 'Other Styles'),
(NULL, 'Low Alcohol', 'Other Styles'),
(NULL, 'Sahti/Gotlandsdricke/Koduõlu', 'Other Styles'),
(NULL, 'Smoked', 'Other Styles'),
(NULL, 'Specialty Grain', 'Other Styles'),
(NULL, 'Spice/Herb/Vegetable', 'Other Styles'),
(NULL, 'Traditional Ale', 'Other Styles');


#
# Dumping data for table 'beers'
#

INSERT INTO beers (id, picture, label, style_id, abv, brewery_id, description, is_deleted)
VALUES 
(NULL, 'pic1', 'Zagorka Spetsialno', 59, 5.0, 12, 'Born with a taste for quality, Zagorka is indeed the only special beer in Bulgaria ...', 0),
(NULL, 'pic2', 'Zagorka Retro', 59, 4.5, 12, 'Zagorka Retro is a high-quality fresh non-pasteurized beer ...', 0),
(NULL, 'pic3', 'Boliarka Radler', 63, 2.1, 5, 'Radler from beer and lemonade made by Garman recipe ...', 0);

#
# Dumping data for table 'status_types'
#

INSERT INTO status_types (id, status)
VALUES 
(NULL, 'drank'),
(NULL, 'want to drink');

#
# Dumping data for table 'beers_status'
#

INSERT INTO beers_status (id, user_id, beer_id, status_type_id)
VALUES 
(NULL, 1, 1, 1),
(NULL, 1, 2, 2),
(NULL, 2, 1, 1),
(NULL, 2, 2, 1),
(NULL, 3, 3, 1);

#
# Dumping data for table 'beers_ratings'
#

INSERT INTO beers_ratings (id, user_id, beer_id, rating)
VALUES 
(NULL, 1, 1, 4),
(NULL, 1, 2, 5),
(NULL, 1, 3, 1),
(NULL, 2, 1, 5),
(NULL, 2, 2, 2),
(NULL, 3, 3, 5);

#
# Dumping data for table 'tags'
#

INSERT INTO tags (id, tag)
VALUES 
(NULL, '#Super'),
(NULL, '#Pivka'),
(NULL, '#Gold'),
(NULL, '#Best');

#
# Dumping data for table 'beers_tags'
#

INSERT INTO beers_tags (id, beer_id, tag_id)
VALUES 
(NULL, 1, 1),
(NULL, 1, 2),
(NULL, 2, 3),
(NULL, 3, 4);


