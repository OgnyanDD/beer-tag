package com.telerikacademy.beer_tag.controllers.mvc_controllers;

import com.telerikacademy.beer_tag.hellpers.BeerModelMapper;
import com.telerikacademy.beer_tag.models.beer.*;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerDetailsDTO;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerEditDTO;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.MarkBeerDTO;
import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.services.contracts.BeersService;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BeerController.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = ConfigurationPropertiesAutoConfiguration.class)
public class BeerControllerTest {

    static final int BEER_ID = 1;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @MockBean
    BeersService service;

    @MockBean
    BeerModelMapper modelMapper;


    @BeforeTransaction
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }

    @Test
    public void listBeers_Should_ReturnAllBeers() throws Exception {
        this.mockMvc.perform(get("/beer-tag/beers-list").param("sortOrder", "0"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"));
    }

    @Test
    public void listBeers_SortedByRating_Should_SortBeersByRating() throws Exception {
        this.mockMvc.perform(get("/beer-tag/beers-list").param("sortOrder", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"));
    }

    @Test
    public void listBeers_SortedByAlcoholVolume_Should_SortBeersByABV() throws Exception {
        this.mockMvc.perform(get("/beer-tag/beers-list").param("sortOrder", "2"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"));
    }

    @Test
    public void listBeers_SortedAlphabetical_Should_SortBeersAlphabeticaly() throws Exception {
        this.mockMvc.perform(get("/beer-tag/beers-list").param("sortOrder", "3"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"));
    }

    @Test
    public void profilePage_Should_ReturnBeerDetails() throws Exception {
        User user = new User();
        Beer beer = new Beer();
        Brewery brewery = new Brewery();
        Country country = new Country();
        Style style = new Style();

        BeerRating rating = new BeerRating();
        rating.setId(1);
        rating.setUser(user);
        rating.setBeer(beer);
        rating.setRating(3);

        beer.setBeerId(1);
        beer.setPicture(new byte[]{1, 2, 1, 1});
        beer.setLabel("Ariana");
        beer.setStyle(style);
        beer.setABV(4.5);
        beer.setBeersRatingsList(new ArrayList<>());
        List<BeerRating> ratings = beer.getBeersRatingsList();
        ratings.add(rating);
        brewery.setBreweryId(1);
        brewery.setBrewery("ZagorkaAD");
        country.setId(1);
        country.setCountry("Bulgaria");
        brewery.setCountry(country);
        beer.setBrewery(brewery);
        when(service.getBeerById(BEER_ID)).thenReturn(beer);
        this.mockMvc.perform(get("/beer-tag/beer/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"))
                .andExpect(model().attribute("beer", instanceOf(BeerDetailsDTO.class)));
    }

    @Test
    public void register_Should_AddNewBeer() throws Exception {
        this.mockMvc.perform(get("/beer-tag/beer/add-beer"))
                .andExpect(status().isOk());
    }

    @Test
    public void editBeer_Should_EditBeerDetails() throws Exception {

        User user = new User();
        Beer beer = new Beer();
        Brewery brewery = new Brewery();
        Country country = new Country();
        Style style = new Style();

        BeerRating rating = new BeerRating();
        rating.setId(1);
        rating.setUser(user);
        rating.setBeer(beer);
        rating.setRating(3);

        beer.setBeerId(1);
        beer.setPicture(new byte[]{1, 2, 1, 1});
        beer.setLabel("Ariana");
        beer.setStyle(style);
        beer.setABV(4.5);
        beer.setBeersRatingsList(new ArrayList<>());
        List<BeerRating> ratings = beer.getBeersRatingsList();
        ratings.add(rating);
        brewery.setBreweryId(1);
        brewery.setBrewery("ZagorkaAD");
        country.setId(1);
        country.setCountry("Bulgaria");
        brewery.setCountry(country);
        beer.setBrewery(brewery);

        when(service.getBeerById(BEER_ID)).thenReturn(beer);
        this.mockMvc.perform(get("/beer-tag/beer/edit-beer/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"))
                .andExpect(model().attribute("beer", instanceOf(BeerEditDTO.class)))
                .andExpect(model().attribute("breweries", service.getBreweries()))
                .andExpect(model().attribute("styles", service.getStyles()));
    }

    @Test
    public void markBeer_Should_MarkBeer() throws Exception{

        Beer beer = new Beer();
        beer.setBeerId(1);
        beer.setLabel("ZagorkaAD");

        when(service.getBeerById(BEER_ID)).thenReturn(beer);
        this.mockMvc.perform(get("/beer-tag/beer/mark-beer/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"))
                .andExpect(model().attribute("beer", instanceOf(MarkBeerDTO.class)))
                .andExpect(model().attribute("users", service.getUsers()))
                .andExpect(model().attribute("marks", service.getBeerStatus()));
    }
}