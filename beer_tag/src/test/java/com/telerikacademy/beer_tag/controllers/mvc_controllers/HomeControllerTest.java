package com.telerikacademy.beer_tag.controllers.mvc_controllers;

import com.telerikacademy.beer_tag.hellpers.BeerModelMapper;
import com.telerikacademy.beer_tag.services.contracts.BeersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HomeController.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = ConfigurationPropertiesAutoConfiguration.class)
public class HomeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BeersService beersService;

    @Test
    public void intro_Should_ShowIntroPage() throws Exception {
        this.mockMvc.perform(get("/beer-tag/"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout-intro"));
    }

    @Test
    public void listBeers_Should_ListAllBeers() throws Exception {

        this.mockMvc.perform(get("/beer-tag/home").param("sortOrder", "0"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"))
                .andExpect(model().attribute("beers", BeerModelMapper.beerDTOList(beersService.getAll())));
    }
}