package com.telerikacademy.beer_tag.controllers.mvc_controllers;

import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.models.user.UserLogin;
import com.telerikacademy.beer_tag.models.user.UserRole;
import com.telerikacademy.beer_tag.models.user.modelsDTO.*;
import com.telerikacademy.beer_tag.services.contracts.UsersService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserControllerTest {

    private static final int FIRST_USER_ID = 1;
    private static final int USER_ROLE_ID = 1;
    private static final int FIRST_USER_LOGIN_ID = 1;

    @Mock
    private UsersService usersService;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void listUsers_Should_ListAllUsers() throws Exception {

        byte[] photo = {1, 2 ,3};

        List<UserRole> userRoles = new ArrayList<>();
        UserRole userRole = new UserRole();
        userRole.setUserRoleId(USER_ROLE_ID);
        userRole.setRole("user");
        userRoles.add(userRole);

        UserLogin firstUserLogin = new UserLogin();
        firstUserLogin.setUserLoginId(FIRST_USER_LOGIN_ID);
        firstUserLogin.setUserRoles(userRoles);
        firstUserLogin.setUserLoginName("todor1888");
        firstUserLogin.setUserPassword("Password01");

        User first_user= new User();
        first_user.setUserId(FIRST_USER_ID);
        first_user.setUserPhoto(photo);
        first_user.setUserFirstName("Todor");
        first_user.setUserLastName("Petrov");
        first_user.setUserEmail("todor1888@dev.gf");
        first_user.setUserLogin(firstUserLogin);

        List<User> userList = new ArrayList<>();
        userList.add(first_user);

        when(usersService.listAllUsers()).thenReturn(userList);

        mockMvc.perform(get("/beer-tag/users-list"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"))
                .andExpect(model().attribute("users",hasSize(1)));
    }

    @Test
    public void profilePage_Should_ReturnUserProfilePage() throws Exception{

        byte[] photo = {1, 2 ,3};

        List<UserRole> userRoles = new ArrayList<>();
        UserRole userRole = new UserRole();
        userRole.setUserRoleId(USER_ROLE_ID);
        userRole.setRole("user");
        userRoles.add(userRole);

        UserLogin firstUserLogin = new UserLogin();
        firstUserLogin.setUserLoginId(FIRST_USER_LOGIN_ID);
        firstUserLogin.setUserRoles(userRoles);
        firstUserLogin.setUserLoginName("todor1888");
        firstUserLogin.setUserPassword("Password01");

        User first_user= new User();
        first_user.setUserId(FIRST_USER_ID);
        first_user.setUserPhoto(photo);
        first_user.setUserFirstName("Todor");
        first_user.setUserLastName("Petrov");
        first_user.setUserEmail("todor1888@dev.gf");
        first_user.setUserLogin(firstUserLogin);

        when(usersService.getUserById(first_user.getUserId())).thenReturn(first_user);

        mockMvc.perform(get("/beer-tag/user-profile/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"))
                .andExpect(model().attribute("user", instanceOf(UserProfileDTO.class)));
    }

    @Test
    public void register_Should_RegisterUser() throws Exception{

        mockMvc.perform(get("/beer-tag/register"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"));
    }

    @Test
    public void editUser_Should_EditUser() throws Exception{
        byte[] photo = {1, 2 ,3};

        List<UserRole> userRoles = new ArrayList<>();
        UserRole userRole = new UserRole();
        userRole.setUserRoleId(USER_ROLE_ID);
        userRole.setRole("user");
        userRoles.add(userRole);

        UserLogin firstUserLogin = new UserLogin();
        firstUserLogin.setUserLoginId(FIRST_USER_LOGIN_ID);
        firstUserLogin.setUserRoles(userRoles);
        firstUserLogin.setUserLoginName("todor1888");
        firstUserLogin.setUserPassword("Password01");

        User first_user= new User();
        first_user.setUserId(FIRST_USER_ID);
        first_user.setUserPhoto(photo);
        first_user.setUserFirstName("Todor");
        first_user.setUserLastName("Petrov");
        first_user.setUserEmail("todor1888@dev.gf");
        first_user.setUserLogin(firstUserLogin);

        when(usersService.getUserById(first_user.getUserId())).thenReturn(first_user);

        mockMvc.perform(get("/beer-tag/user/edit-user/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"))
                .andExpect(model().attribute("user", instanceOf(UserEditDTO.class)));
    }

    @Test
    public void changeUserRole_Should_ChangeUserRole() throws Exception{
        byte[] photo = {1, 2 ,3};

        List<UserRole> userRoles = new ArrayList<>();
        UserRole userRole = new UserRole();
        userRole.setUserRoleId(USER_ROLE_ID);
        userRole.setRole("user");
        userRoles.add(userRole);

        UserLogin firstUserLogin = new UserLogin();
        firstUserLogin.setUserLoginId(FIRST_USER_LOGIN_ID);
        firstUserLogin.setUserRoles(userRoles);
        firstUserLogin.setUserLoginName("todor1888");
        firstUserLogin.setUserPassword("Password01");

        User first_user= new User();
        first_user.setUserId(FIRST_USER_ID);
        first_user.setUserPhoto(photo);
        first_user.setUserFirstName("Todor");
        first_user.setUserLastName("Petrov");
        first_user.setUserEmail("todor1888@dev.gf");
        first_user.setUserLogin(firstUserLogin);

        when(usersService.getUserById(first_user.getUserId())).thenReturn(first_user);

        mockMvc.perform(get("/beer-tag/user/change-role/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("base-layout"))
                .andExpect(model().attribute("user", instanceOf(ChangeUserRoleDTO.class)))
                .andExpect(model().attribute("roles", usersService.getUserRoles()));
    }
}