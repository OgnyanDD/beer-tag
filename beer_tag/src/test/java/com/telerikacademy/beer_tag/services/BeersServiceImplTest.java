package com.telerikacademy.beer_tag.services;

import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerEditDTO;
import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.repositories.SqlBeerRepository;
import com.telerikacademy.beer_tag.repositories.contracts.BeersRepository;
import com.telerikacademy.beer_tag.services.contracts.BeersService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class BeersServiceImplTest {

    static final int BEER_ID = 1;

    BeersRepository mockRepository = Mockito.mock(SqlBeerRepository.class);

    BeersService service = new BeersServiceImpl(mockRepository);

    @Before
    public void setUp() throws Exception {
        Beer beer = new Beer();
        beer.setBeerId(BEER_ID);
        beer.setLabel("Zagorka");

        Beer beer1 = new Beer();
        beer1.setBeerId(2);
        beer1.setLabel("Ariana");
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        beerList.add(beer1);

        User user = new User();
        user.setUserId(1);

        List<Beer> sortedByAlcoholVolume = beerList.stream()
                .sorted(Comparator.comparingDouble(Beer::getABV).reversed())
                .collect(Collectors.toList());

        List<Beer> sortedAlphabetical = beerList.stream()
                .sorted(Comparator.comparing(Beer::getLabel).reversed()).collect(Collectors.toList());

        when(mockRepository.getAll()).thenReturn(beerList);
        when(mockRepository.getBeerById(BEER_ID)).thenReturn(beer);
        when(mockRepository.sortBeerByRating()).thenReturn(beerList);
        when(mockRepository.sortByAlcoholVolume()).thenReturn(sortedByAlcoholVolume);
        when(mockRepository.sortBeerAlphabetically()).thenReturn(sortedAlphabetical);
    }

    @Test
    public void getAll_Should_GetAllBeers() {

        List<Beer> allBeers = service.getAll();
        Assert.assertEquals(2, allBeers.size());
        verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getBeerById_Should_GetBeerById() {
        Beer result = service.getBeerById(1);
        Assert.assertEquals(1, result.getBeerId());
        verify(mockRepository, Mockito.times(1)).getBeerById(1);

    }

    @Test
    public void sortBeerByRating_Should_SortBeersByRating() {

        List<Beer> result = service.sortBeerByRating();
        Assert.assertEquals(2, result.size());
        Mockito.verify(mockRepository, Mockito.times(1)).sortBeerByRating();

    }

    @Test
    public void sortByAlcoholVolume_Should_SortBeerByABV() {

        List<Beer> result = service.sortByAlcoholVolume();
        Assert.assertEquals(2, result.size());
        Mockito.verify(mockRepository, Mockito.times(1)).sortByAlcoholVolume();
    }

    @Test
    public void sortAlphabetically_Should_SortBeersAlphabeticaly() {

        List<Beer> result = service.sortBeerAlphabetically();
        Assert.assertEquals(2, result.size());
        Mockito.verify(mockRepository, Mockito.times(1)).sortBeerAlphabetically();
    }


    @Test
    public void filterByStyle_Should_FilterBeersByStyle() {
        List<Beer> result = service.filterBeerByStyle(1);
        Assert.assertEquals(0, result.size());
        verify(mockRepository, Mockito.times(1)).filterBeerByStyle(1);
    }

    @Test
    public void byCountry_Should_FilterBeersByCountry() {

        List<Beer> result = service.filterByCountry(25);
        Assert.assertEquals(0, result.size());
        verify(mockRepository, Mockito.times(1)).filterBeerByCountry(25);
    }

    @Test
    public void tagBeer_Should_Should_TagBeer() {

        service.tagBeer(BEER_ID, "GoodBeers");
        verify(mockRepository, Mockito.times(1)).tagBeer(BEER_ID, "GoodBeers");
    }

    @Test
    public void addNew_Should_AddNewBeer() {
        Beer beer = new Beer();
        service.addNew(beer);
        verify(mockRepository, Mockito.times(1)).addBeer(beer);
    }

    @Test
    public void deleteBeer_Should_DeleteBeer() {
        Beer beer = new Beer();
        beer.setBeerId(3);
        service.deleteBeer(3);
        verify(mockRepository, Mockito.times(1)).deleteBeer(3);
    }

    @Test
    public void voteForBeer_Should_VoteForBeer() {
        int userId = 1;
        int beerId = BEER_ID;
        int points = 3;

        service.voteForBeer(userId, beerId, points);
        verify(mockRepository, Mockito.times(1)).voteForBeer(userId, beerId, points);
    }

    @Test
    public void editBeer_Should_EditBeerDetails() {
        BeerEditDTO beerEditDTO = new BeerEditDTO();
        service.editBeer(beerEditDTO);
        verify(mockRepository, Mockito.times(1)).editBeer(beerEditDTO);
    }

    @Test
    public void filterByTag_Should_FilterByTag() {

        service.filterByTag("tag");
        verify(mockRepository, Mockito.times(1)).filterBeerByTag("tag");
    }

    @Test
    public void getStyles_Should_GetAllBeerStyles() {
        service.getStyles();
        verify(mockRepository,Mockito.times(1)).getStyles();
    }

    @Test
    public  void getBreweries_Should_GetBreweries() {
        service.getBreweries();
        verify(mockRepository,Mockito.times(1)).getBreweries();
    }

    @Test
    public void getCountries_Should_GetCountries(){
        service.getCountries();
        verify(mockRepository,Mockito.times(1)).getCountries();
    }

    @Test
    public void markBeer_Should_MarkBeer() {
        int userId = 1;
        int beerId = BEER_ID;
        int statusId = 1;
        service.markBeer(userId,beerId,statusId);
        verify(mockRepository,Mockito.times(1)).markBeer(userId,beerId,statusId);
    }

    @Test
    public void getUsers_Should_GetAllUsers() {
        service.getUsers();
        verify(mockRepository,Mockito.times(1)).getUsers();
    }

    @Test
    public  void getStyleById_Should_GetBeerStyleById() {

        int id = 1;
        service.getStyleById(id);
        verify(mockRepository,Mockito.times(1)).getStyleById(id);
    }

    @Test
    public void getBreweryById_Should_GetBreweryById() {
        int id = 1;
        service.getBreweryById(id);
        verify(mockRepository,Mockito.times(1)).getBreweryById(id);
    }

    @Test
    public void getBeerStatus_Should_GetBeerStatus() {
        service.getBeerStatus();
        verify(mockRepository,Mockito.times(1)).getBeerStatus();
    }
}