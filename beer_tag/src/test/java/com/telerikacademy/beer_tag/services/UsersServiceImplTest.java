package com.telerikacademy.beer_tag.services;

import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.beer.BeerRating;
import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.models.user.UserLogin;
import com.telerikacademy.beer_tag.models.user.UserRole;
import com.telerikacademy.beer_tag.models.user.modelsDTO.UserEditDTO;
import com.telerikacademy.beer_tag.repositories.SqlUserRepository;
import com.telerikacademy.beer_tag.repositories.contracts.UsersRepository;
import com.telerikacademy.beer_tag.services.contracts.UsersService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UsersServiceImplTest {

    private static final int FIRST_USER_ID = 1;
    private static final int SECOND_USER_ID = 2;

    private static final int USER_ROLE_ID = 1;
    private static final int FIRST_USER_LOGIN_ID = 1;
    private static final int SECOND_USER_LOGIN_ID = 2;

    private static final int BEER_RATING_ID = 1;
    private static final int BEER_ID = 1;

    private UsersRepository mockRepository = Mockito.mock(SqlUserRepository.class);

    private UsersService service = new UsersServiceImpl(mockRepository);

    @Before
    public void setUp() {

        byte[] photo = {1, 2 ,3};

        List<UserRole> userRoles = new ArrayList<>();
        UserRole userRole = new UserRole();
        userRole.setUserRoleId(USER_ROLE_ID);
        userRole.setRole("user");
        userRoles.add(userRole);

        UserLogin firstUserLogin = new UserLogin();
        firstUserLogin.setUserLoginId(FIRST_USER_LOGIN_ID);
        firstUserLogin.setUserRoles(userRoles);
        firstUserLogin.setUserLoginName("todor1888");
        firstUserLogin.setUserPassword("Password01");

        UserLogin secondUserLogin = new UserLogin();
        firstUserLogin.setUserLoginId(SECOND_USER_LOGIN_ID);
        firstUserLogin.setUserRoles(userRoles);
        firstUserLogin.setUserLoginName("mary1999");
        firstUserLogin.setUserPassword("Password02");

        User first_user= new User();
        first_user.setUserId(FIRST_USER_ID);
        first_user.setUserPhoto(photo);
        first_user.setUserFirstName("Todor");
        first_user.setUserLastName("Petrov");
        first_user.setUserEmail("todor1888@dev.gf");
        first_user.setUserLogin(firstUserLogin);

        User second_user= new User();
        second_user.setUserId(SECOND_USER_ID);
        second_user.setUserPhoto(photo);
        second_user.setUserFirstName("Maria");
        second_user.setUserLastName("Ivanova");
        second_user.setUserEmail("mary99@dev.gf");
        first_user.setUserLogin(secondUserLogin);

        List<User> userList = new ArrayList<>();
        userList.add(first_user);
        userList.add(second_user);

        Beer beer = new Beer();
        beer.setBeerId(BEER_ID);
        BeerRating beerRating = new BeerRating();
        beerRating.setId(BEER_RATING_ID);
        beerRating.setBeer(beer);
        beerRating.setUser(first_user);
        beerRating.setRating(5);
        List<BeerRating> beerRatings = new ArrayList<>();
        beerRatings.add(beerRating);

        first_user.setBeersRatingsList(beerRatings);

        List<Beer> beers = first_user.getBeersRatingsList()
                .stream()
                .limit(3)
                .map(BeerRating::getBeer)
                .collect(Collectors.toList());

        when(mockRepository.listAllUsers()).thenReturn(userList);
        when(mockRepository.getById(FIRST_USER_ID)).thenReturn(first_user);
        when(mockRepository.getUserRoles()).thenReturn(userRoles);
        when(mockRepository.getUserFavBeers(FIRST_USER_ID)).thenReturn(beers);
    }


    @Test
    public void listAllUsers_Should_ListAllUsers() {

        List<User> allUsers = service.listAllUsers();
        Assert.assertEquals(2, allUsers.size());
        verify(mockRepository, Mockito.times(1)).listAllUsers();

    }

    @Test
    public void getUserById_Should_GetUser() {

        User result = service.getUserById(FIRST_USER_ID);
        Assert.assertEquals(1, result.getUserId());
        verify(mockRepository, Mockito.times(1)).getById(FIRST_USER_ID);
    }

    @Test
    public void createNewUser_Should_RegisterNewUser() {

        User user = new User();
        service.createNewUser(user);
        verify(mockRepository, Mockito.times(1)).createNewUser(user);
    }

    @Test
    public void editUserInfo_Should_EditUserDetails() {
        UserEditDTO userEditDTO = new UserEditDTO();
        service.editUserInfo(userEditDTO);
        verify(mockRepository, Mockito.times(1)).editUserInfo(userEditDTO);
    }

    @Test
    public void editUserRole_Should_ChangeUserRole() {
        User user = new User();
        user.setUserId(FIRST_USER_ID);
        service.editUserRole(user.getUserId(), USER_ROLE_ID);
        verify(mockRepository, Mockito.times(1)).editUserRole(FIRST_USER_ID, USER_ROLE_ID);
    }

    @Test
    public void getDefaultRole_Should_GetDefaultUserRole() {

        service.getDefaultRole();
        verify(mockRepository, Mockito.times(1)).getDefaultRole();
    }

    @Test
    public void getUserRoles_Should_GetUsersRoles() {
        List<UserRole> userRoles = service.getUserRoles();
        Assert.assertEquals(1, userRoles.size());
        verify(mockRepository, Mockito.times(1)).getUserRoles();
    }

    @Test
    public void getUserFavBeers_Should_GetUserFavBeers() {
        List<Beer> result = service.getUserFavBeers(FIRST_USER_ID);
        Assert.assertEquals(1, result.size());
        Mockito.verify(mockRepository, Mockito.times(1)).getUserFavBeers(FIRST_USER_ID);

    }

    @Test
    public void deleteUser_Should_DeleteUser() {

        User user = new User();
        user.setUserId(FIRST_USER_ID);
        service.deleteUser(user.getUserId());
        verify(mockRepository, Mockito.times(1)).deleteUser(FIRST_USER_ID);
    }
}