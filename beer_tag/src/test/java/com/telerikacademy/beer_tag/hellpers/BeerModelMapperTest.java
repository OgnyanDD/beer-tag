package com.telerikacademy.beer_tag.hellpers;

import com.telerikacademy.beer_tag.models.beer.*;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.*;
import com.telerikacademy.beer_tag.models.user.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BeerModelMapper.class)
public class BeerModelMapperTest {

    private User user = new User();
    private Beer beer = new Beer();
    private Beer beer1 = new Beer();

    private Brewery brewery = new Brewery();
    private Country country = new Country();

    private Style style = new Style();


    @Before
    public void setUp() throws Exception {
        user.setUserId(1);
        style.setType("Lager");
        style.setId(1);

        BeerRating rating = new BeerRating();
        rating.setId(1);
        rating.setUser(user);
        rating.setBeer(beer);
        rating.setRating(3);

        BeerRating rating1 = new BeerRating();
        rating1.setRating(2);
        rating1.setUser(user);
        rating1.setBeer(beer1);
        rating1.setRating(4);

        beer.setBeerId(1);
        beer.setPicture(new byte[]{1, 2, 1, 1});
        beer.setLabel("Ariana");
        beer.setStyle(style);
        beer.setABV(4.5);
        beer.setBeersRatingsList(new ArrayList<>());
        List<BeerRating> ratings = beer.getBeersRatingsList();
        ratings.add(rating);
        brewery.setBreweryId(1);
        brewery.setBrewery("ZagorkaAD");
        country.setId(1);
        country.setCountry("Bulgaria");
        brewery.setCountry(country);
        beer.setBrewery(brewery);
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);

        beer1.setBeerId(2);
        beer1.setPicture(new byte[]{1, 2, 1, 1});
        beer1.setLabel("Zagorka");
        beer1.setStyle(style);
        beer1.setABV(4.7);
        beer1.setBeersRatingsList(new ArrayList<>());
        List<BeerRating> ratings1 = beer1.getBeersRatingsList();
        ratings1.add(rating1);
        brewery.setBreweryId(1);
        brewery.setBrewery("ZagorkaAD");
        country.setId(1);
        country.setCountry("Bulgaria");
        brewery.setCountry(country);
        beer1.setBrewery(brewery);
        beerList.add(beer);
    }

    @Test
    public void toDTO_Should_MapBeerDetailsToDTO() {

        BeerDetailsDTO beerDetailsDTO = BeerModelMapper.toDTO(beer);
        Assert.assertEquals(1, beerDetailsDTO.getId());
    }

    @Test
    public void beerDTOList_Should_MapBeerDetailsForListOfAllBeers() {
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        List<BeerListDTO> beerListDTOS = BeerModelMapper.beerDTOList(beerList);
        Assert.assertEquals(1,beerListDTOS.size());

    }

    @Test
    public void sortedBeersByRating_Should_MapBeerForBeerRating() {
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        beerList.add(beer1);
        List<BeerListDTO> beerListDTOS = BeerModelMapper.sortedBeersByRating(beerList);
        Assert.assertEquals(2,beerListDTOS.size());
        Assert.assertEquals(1,beerList.get(0).getBeerId());

    }

    @Test
    public void indexPageDTO_Should_MapBeerForIndexPage() {
        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);
        beerList.add(beer1);

        List<BeerIndexPageDTO> beerIndexPageDTOList = BeerModelMapper.indexPageDTO(beerList);
        Assert.assertEquals(2,beerIndexPageDTOList.size());
    }

    @Test
    public void editBeer_Should_MapBeerEditDetailsToDTO() {

        BeerEditDTO  beerEditDTO=BeerModelMapper.editBeer(beer1);
        Assert.assertEquals(2,beerEditDTO.getId());
        Assert.assertEquals("Zagorka",beerEditDTO.getLabel());
    }

    @Test
    public void roundRating_Should_RoundBeerRating() {
        double rate = 1.522;
        double roundRate = BeerModelMapper.roundRating(rate);
        Assert.assertEquals(1.60,roundRate,rate);
    }

    @Test
    public void markBeer_Should_MapBeerToMarkBeerDTO() {

        MarkBeerDTO markBeerDTO = BeerModelMapper.markBeer(beer);
        assertEquals(1,markBeerDTO.getId());

    }

    @Test
    public void tagBeer_Should_MapBeerTagDTO() {

        TagBeerDTO tagBeerDTO = BeerModelMapper.tagBeer(beer1);
        Assert.assertEquals(2,tagBeerDTO.getId());
    }
}