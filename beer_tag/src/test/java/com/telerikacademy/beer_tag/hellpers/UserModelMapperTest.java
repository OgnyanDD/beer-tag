package com.telerikacademy.beer_tag.hellpers;

import com.telerikacademy.beer_tag.models.beer.*;
import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.models.user.UserLogin;
import com.telerikacademy.beer_tag.models.user.UserRole;
import com.telerikacademy.beer_tag.models.user.modelsDTO.*;
import com.telerikacademy.beer_tag.repositories.SqlBeerRepository;
import com.telerikacademy.beer_tag.repositories.SqlUserRepository;
import com.telerikacademy.beer_tag.repositories.contracts.BeersRepository;
import com.telerikacademy.beer_tag.repositories.contracts.UsersRepository;
import com.telerikacademy.beer_tag.services.BeersServiceImpl;
import com.telerikacademy.beer_tag.services.UsersServiceImpl;
import com.telerikacademy.beer_tag.services.contracts.BeersService;
import com.telerikacademy.beer_tag.services.contracts.UsersService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserModelMapperTest {

    private static final int FIRST_USER_ID = 1;
    private static final int USER_ROLE_ID = 1;
    private static final int FIRST_USER_LOGIN_ID = 1;

    private static final int BEER_ID = 1;
    private static final int STYLE_ID = 1;
    private static final int BREWERY_ID = 1;
    private static final int COUNTRY_ID = 1;
    private static final int BEER_RATING_ID = 1;
    private static final int BEER_STATUS_ID = 1;
    private static final int BEER_STATUS_TYPE_ID = 1;

    private UsersRepository mockUserRepository = Mockito.mock(SqlUserRepository.class);
    private BeersRepository mockBeerRepository = Mockito.mock(SqlBeerRepository.class);

    private UsersService userService = new UsersServiceImpl(mockUserRepository);
    private BeersService beerService = new BeersServiceImpl(mockBeerRepository);

    @Before
    public void setUp() {
        byte[] photo = {1, 2 ,3};

        List<UserRole> userRoles = new ArrayList<>();
        UserRole userRole = new UserRole();
        userRole.setUserRoleId(USER_ROLE_ID);
        userRole.setRole("user");
        userRoles.add(userRole);

        UserLogin firstUserLogin = new UserLogin();
        firstUserLogin.setUserLoginId(FIRST_USER_LOGIN_ID);
        firstUserLogin.setUserRoles(userRoles);
        firstUserLogin.setUserLoginName("todor1888");
        firstUserLogin.setUserPassword("Password01");

        User first_user= new User();
        first_user.setUserId(FIRST_USER_ID);
        first_user.setUserPhoto(photo);
        first_user.setUserFirstName("Todor");
        first_user.setUserLastName("Petrov");
        first_user.setUserEmail("todor1888@dev.gf");
        first_user.setUserLogin(firstUserLogin);

        List<User> userList = new ArrayList<>();
        userList.add(first_user);

        Style style = new Style();
        style.setId(STYLE_ID);
        style.setType("Cream Ale");
        style.setCategory("Anglo-American Ales");

        Country country = new Country();
        country.setId(COUNTRY_ID);
        country.setCountry("Bulgaria");

        Brewery brewery = new Brewery();
        brewery.setBreweryId(BREWERY_ID);
        brewery.setBrewery("Zavod Zagorka");
        brewery.setCountry(country);

        Beer beer = new Beer();
        beer.setBeerId(BEER_ID);
        beer.setLabel("Zagorka");
        beer.setStyle(style);
        beer.setABV(5.0);
        beer.setBrewery(brewery);

        BeerRating beerRating = new BeerRating();
        beerRating.setId(BEER_RATING_ID);
        beerRating.setUser(first_user);
        beerRating.setBeer(beer);
        beerRating.setRating(5);

        List<BeerRating> beerRatings = new ArrayList<>();
        beerRatings.add(beerRating);

        StatusTypes statusTypes = new StatusTypes();
        statusTypes.setId(BEER_STATUS_TYPE_ID);
        statusTypes.setStatus("drunk");

        BeerStatus beerStatus = new BeerStatus();
        beerStatus.setId(BEER_STATUS_ID);
        beerStatus.setUser(first_user);
        beerStatus.setBeer(beer);
        beerStatus.setStatus(statusTypes);

        List<BeerStatus> beerStatuses = new ArrayList<>();
        beerStatuses.add(beerStatus);

        beer.setBeersRatingsList(beerRatings);
        beer.setBeersStatusesList(beerStatuses);

        List<Beer> beerList = new ArrayList<>();
        beerList.add(beer);

        when(mockUserRepository.getById(FIRST_USER_ID)).thenReturn(first_user);
        when(mockUserRepository.listAllUsers()).thenReturn(userList);
        when(mockBeerRepository.getAll()).thenReturn(beerList);
    }

    @Test
    public void changeUserRole_Should_MapUserRoleToDTO() {

        ChangeUserRoleDTO user = UserModelMapper.changeUserRole(userService.getUserById(FIRST_USER_ID));
        Assert.assertEquals(1, user.getId());
    }

    @Test
    public void setUserProfileDTO_Should_MapUserProfileToDTO() {
        UserProfileDTO user = UserModelMapper.setUserProfileDTO(userService.getUserById(FIRST_USER_ID));
        Assert.assertEquals(1, user.getId());
    }

    @Test
    public void editUserProfileDTO_Should_MapEditUserProfileToDTO() {
        UserEditDTO user = UserModelMapper.editUserProfileDTO(userService.getUserById(FIRST_USER_ID));
        Assert.assertEquals(1, user.getUserId());
    }

    @Test
    public void setUserListInfoDTO_Should_MapUsersInfoListToDTO() {
        List<UsersListDTO> users = UserModelMapper.setUserListInfoDTO(userService.listAllUsers());
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void getUserFavBeers_Should_MapUserFavBeerListToDTO() {

        List<UserFavBeersDTO> favBeers = UserModelMapper.getUserFavBeers(beerService.getAll(), FIRST_USER_ID);
        Assert.assertEquals(1, favBeers.size());
    }
}