package com.telerikacademy.beer_tag.exceptions;

public class DatabaseItemNotFoundException extends RuntimeException {

    public DatabaseItemNotFoundException(int id) {
        super(String.format("Entity with ID %d not found.", id));
    }
}

