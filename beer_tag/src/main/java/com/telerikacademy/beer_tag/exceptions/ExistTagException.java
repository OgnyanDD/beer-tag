package com.telerikacademy.beer_tag.exceptions;

public class ExistTagException extends RuntimeException{
    public ExistTagException(String tag){

        super(String.format("This beer is already tagged whit tag %s!", tag));
    }
}