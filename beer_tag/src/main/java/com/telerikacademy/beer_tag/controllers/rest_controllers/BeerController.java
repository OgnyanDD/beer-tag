package com.telerikacademy.beer_tag.controllers.rest_controllers;

import com.telerikacademy.beer_tag.exceptions.DatabaseItemNotFoundException;
import com.telerikacademy.beer_tag.exceptions.ExistTagException;
import com.telerikacademy.beer_tag.hellpers.BeerModelMapper;
import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerDetailsDTO;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerEditDTO;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerListDTO;
import com.telerikacademy.beer_tag.services.contracts.BeersService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

//@RestController
@RequestMapping("/beer-tag/beers")

public class BeerController {
    private BeersService beersService;

    @Autowired
    public BeerController(BeersService beersService) {
        this.beersService = beersService;
    }

    @GetMapping
    public List<BeerListDTO> getAllBeers() {
        try {
            return BeerModelMapper.beerDTOList(beersService.getAll());
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        }
    }

    @GetMapping("/{id}")
    public BeerDetailsDTO getBeerById(@PathVariable int id) {
        try {
            return BeerModelMapper.toDTO(beersService.getBeerById(id));

        } catch (RuntimeException ex) {
            throw new ResponseStatusException
                    (HttpStatus.BAD_REQUEST, String.format("Beer whit Id %d not found!", id));
        }
    }

    @PostMapping("/new")
    void addNewBeer(@RequestBody Beer beer) {
        try {
            beersService.addNew(beer);
        } catch (HibernateException he) {
            throw new ResponseStatusException
                    (HttpStatus.INTERNAL_SERVER_ERROR, "Failed to access database.");
        }
    }

    @GetMapping("/sortAlphabetically")
    public List<BeerListDTO> sortBeerAlphabetically() {
        try {
            return BeerModelMapper.beerDTOList(beersService.sortBeerAlphabetically());

        } catch (HibernateException he) {
            throw new ResponseStatusException
                    (HttpStatus.INTERNAL_SERVER_ERROR, "Failed to access database.");
        }
    }

    @GetMapping("/sortByAlcoholVolume")
    public List<BeerListDTO> sortByAlcoholVolume() {
        try {
            return BeerModelMapper.beerDTOList(beersService.sortByAlcoholVolume());

        } catch (HibernateException he) {
            throw new ResponseStatusException
                    (HttpStatus.INTERNAL_SERVER_ERROR, "Failed to access database.");
        }
    }

    @PostMapping("/deleteBeer/{id}")
    void deleteBeer(@PathVariable int id) {
        try {
            beersService.deleteBeer(id);

        } catch (HibernateException he) {
            throw new ResponseStatusException
                    (HttpStatus.NOT_FOUND, String.format("Beer whit id %d not Found!", id));
        }
    }

    @PostMapping("/voteForBeer/{userId}/{beerId}/{points}")
    void voteForBeer(@Valid @PathVariable int userId, @PathVariable int beerId, @PathVariable int points) {
        try {
            beersService.voteForBeer(userId, beerId, points);

        } catch (HibernateException he) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Item not found!");
        }
    }

    @PutMapping("editBeerInfo/{beerId}")
    void editBeerInfo(@Valid @RequestBody BeerEditDTO beetUpdate) {
        try {
            beersService.editBeer(beetUpdate);
        } catch (HibernateException he) {
            throw new ResponseStatusException
                    (HttpStatus.NOT_FOUND, String.format("Beer not found!"));
        }
    }

    @GetMapping("filterByTag/{tag}")
    public List<BeerListDTO> filterByTag(@PathVariable String tag) {
        try {
            return BeerModelMapper.beerDTOList(beersService.filterByTag(tag));

        } catch (HibernateException he) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to access database!");
        }
    }

    @GetMapping("/filterByCountry/{countryId}")
    public List<BeerListDTO> filterByCountry(@PathVariable int countryId) {
        try {
            return BeerModelMapper.beerDTOList(beersService.filterByCountry(countryId));
        } catch (HibernateException he) {
            throw new ResponseStatusException
                    (HttpStatus.INTERNAL_SERVER_ERROR, "Failed access to database!");
        }
    }

    @GetMapping("/filterByStyle/{styleId}")
    public List<BeerListDTO> filterBeerByStyle(@PathVariable int styleId) {
        try {
            return BeerModelMapper.beerDTOList(beersService.filterBeerByStyle(styleId));

        } catch (HibernateException he) {
            throw new ResponseStatusException
                    (HttpStatus.INTERNAL_SERVER_ERROR, "Failed access to database!");
        }
    }

    @GetMapping("/sortBeerByRating")
    public List<BeerListDTO> sortBeerByRating() {
        try {
            return BeerModelMapper.sortedBeersByRating(beersService.sortBeerByRating());

        } catch (HibernateException he) {
            throw new ResponseStatusException
                    (HttpStatus.INTERNAL_SERVER_ERROR, "Failed access to database!");
        }
    }

    @PostMapping("/tagBeer/{beerId}/{tag}")
    public void tagBeer(@Valid @PathVariable int beerId, @PathVariable String tag) {
        try {
            beersService.tagBeer(beerId, tag);

        } catch (HibernateException he) {

            throw new ResponseStatusException
                    (HttpStatus.NOT_FOUND, String.format("Beer whit id %d not found!", beerId));
        } catch (ExistTagException ex) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/user/markBeer/{userId}/{beerId}/{statusId}")
    void markBeer(@PathVariable int userId, @PathVariable int beerId, @PathVariable int statusId) {
        try {
            beersService.markBeer(userId, beerId, statusId);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        } catch (DatabaseItemNotFoundException de) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    de.getMessage()
            );
        }
    }
}
