package com.telerikacademy.beer_tag.controllers.mvc_controllers;

import com.telerikacademy.beer_tag.hellpers.BeerModelMapper;
import com.telerikacademy.beer_tag.services.contracts.BeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/beer-tag")
public class HomeController {

    private BeersService beersService;

    @Autowired
    public HomeController(BeersService beersService) {
        this.beersService = beersService;
    }

    @GetMapping("/")
    public String intro(Model model) {
        model.addAttribute("intro", "index/intro");

        return "base-layout-intro";
    }

    @GetMapping("/home")
    public String ListBeers(Model model, @RequestParam(name = "sortOrder", required = false, defaultValue = "0") Integer sortOrder) {

        model.addAttribute("beers", BeerModelMapper.indexPageDTO(beersService.getAll()));
        model.addAttribute("view", "index/home");

        return "base-layout";
    }
}
