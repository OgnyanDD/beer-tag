package com.telerikacademy.beer_tag.controllers.mvc_controllers;

import com.telerikacademy.beer_tag.hellpers.UserModelMapper;
import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.user.*;
import com.telerikacademy.beer_tag.models.user.modelsDTO.*;
import com.telerikacademy.beer_tag.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/beer-tag")
public class UserController {

    private UsersService usersService;

    @Autowired
    public UserController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/users-list")
    public String listUsers(Model model) {

        model.addAttribute("users", UserModelMapper.setUserListInfoDTO(usersService.listAllUsers()));
        model.addAttribute("view", "user/users");

        return "base-layout";
    }

    @GetMapping("/user-profile/{id:\\d+}")
    public String profilePage(Model model, @PathVariable int id) {

        model.addAttribute("user", UserModelMapper.setUserProfileDTO(usersService.getUserById(id)));
        model.addAttribute("view", "user/profile");

        return "base-layout";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("view", "user/register");

        return "base-layout";
    }

    @PostMapping("/register")
    public String registerProcess(UserRegisterDTO newUser) {

        if (!newUser.getPassword().equals(newUser.getConfirmPassword())) {
            return "redirect:/beer-tag/register";
        }

        UserRole role = usersService.getDefaultRole();
        UserLogin login = new UserLogin();

        login.setUserLoginName(newUser.getUserName());
        login.setUserPassword(newUser.getPassword());
        List<UserRole> userRoleList = login.getUserRoles();
        userRoleList.add(role);

        User user = new User();
        user.setUserFirstName(newUser.getFirstName());
        user.setUserLastName(newUser.getLastName());
        user.setUserEmail(newUser.getEmail());
        user.setUserLogin(login);

        try {
            user.setUserPhoto(newUser.getPicture().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        usersService.createNewUser(user);

        return "redirect:/beer-tag/login";
    }

    @GetMapping("/user/edit-user/{id:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String editUser(Model model, @PathVariable int id) {

        model.addAttribute("user", UserModelMapper.editUserProfileDTO(usersService.getUserById(id)));
        model.addAttribute("view", "user/edit-user");

        return "base-layout";
    }

    @PostMapping("/user/edit-user")
//    @PreAuthorize("isAuthenticated()")
    public String editProcess(@ModelAttribute UserEditDTO userEdit) {

        this.usersService.editUserInfo(userEdit);

        return "redirect:/beer-tag/users-list/";
    }

    @GetMapping("/user/change-role/{idUser:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String changeUserRole(Model model, @PathVariable int idUser) {

        model.addAttribute("user", UserModelMapper.changeUserRole(usersService.getUserById(idUser)));
        model.addAttribute("roles", usersService.getUserRoles());
        model.addAttribute("view", "user/change-role");

        return "base-layout";
    }

    @PostMapping("/user/change-role/{idUser:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String changeRoleProcess(@PathVariable int idUser, ChangeUserRoleDTO changeUserRoleDTO) {

        User user = usersService.getUserById(idUser);

        usersService.editUserRole(user.getUserId(), changeUserRoleDTO.getRoleId());

        return "redirect:/beer-tag/users-list/";
    }

    @GetMapping("/user/favorite-beers/{id:\\d+}")
    public String userFavBeers(Model model, @PathVariable int id) {

        List<Beer> beers = usersService.getUserFavBeers(id);
        List<UserFavBeersDTO> favBeers = UserModelMapper.getUserFavBeers(beers, id);
        model.addAttribute("beers", favBeers);
        model.addAttribute("view", "user/fav-beers");

        return "base-layout";
    }

    @PostMapping("/user/delete-user/{id:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String deleteProcess(@PathVariable int id) {

        this.usersService.deleteUser(id);

        return "redirect:/beer-tag/users-list/";
    }

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("view", "user/login");

        return "base-layout";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//
//        if (auth != null) {
//            new SecurityContextLogoutHandler().logout(request, response, auth);
//        }

        return "redirect:/login?logout";
    }
}
