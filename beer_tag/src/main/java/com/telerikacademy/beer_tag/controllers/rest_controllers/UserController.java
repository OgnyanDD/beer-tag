package com.telerikacademy.beer_tag.controllers.rest_controllers;

import com.telerikacademy.beer_tag.exceptions.DatabaseItemNotFoundException;
import com.telerikacademy.beer_tag.hellpers.UserModelMapper;
import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.models.user.modelsDTO.*;
import com.telerikacademy.beer_tag.services.contracts.UsersService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

//@RestController
@RequestMapping("/beer-tag")
public class UserController {

    private UsersService usersService;

    @Autowired
    public UserController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/users")
    List<UsersListDTO> getAllUsers() {
        try {
            return UserModelMapper.setUserListInfoDTO(usersService.listAllUsers());
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        }
    }

    @GetMapping("/user/{userId}")
    UserProfileDTO getUserById(@PathVariable int userId) {
        try {
            return UserModelMapper.setUserProfileDTO(usersService.getUserById(userId));
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        } catch (EmptyResultDataAccessException de) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    de.getMessage()
            );
        }
    }

    @PostMapping("/user/newUser")
    void createNewUser(@Valid @RequestBody User user) {
        try {
            usersService.createNewUser(user);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        }
    }

    @PutMapping("/user/editUser")
    void editUserInfo(@RequestBody UserEditDTO dto) {
        try {
            usersService.editUserInfo(dto);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        } catch (EmptyResultDataAccessException de) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    de.getMessage()
            );
        }
    }

    @PutMapping("/user/editUserRole/{userId}/{role}")
    void editUserRole(@PathVariable int userId, @PathVariable int role) {
        try {
            usersService.editUserRole(userId, role);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        } catch (DatabaseItemNotFoundException de) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    de.getMessage()
            );
        }
    }

    @GetMapping("/user/favBeerList/{userId}")
    List<Beer> getUserFavBeers(@PathVariable int userId) {
        try {
            return usersService.getUserFavBeers(userId);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        } catch (EmptyResultDataAccessException de) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    de.getMessage()
            );
        }
    }

    @DeleteMapping("/user/deleteUser/{userId}")
    void deleteUser(@PathVariable int userId) {
        try {
            usersService.deleteUser(userId);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database."
            );
        } catch (EmptyResultDataAccessException de) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    de.getMessage()
            );
        }
    }
}
