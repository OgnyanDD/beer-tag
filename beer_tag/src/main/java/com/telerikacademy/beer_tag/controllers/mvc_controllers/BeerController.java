package com.telerikacademy.beer_tag.controllers.mvc_controllers;

import com.telerikacademy.beer_tag.hellpers.BeerModelMapper;

import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.*;
import com.telerikacademy.beer_tag.services.contracts.BeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

@Controller
@RequestMapping("/beer-tag")
public class BeerController {

    private BeersService beersService;

    private LinkedHashMap sortings = new LinkedHashMap() {{
        put(0, "Sort by ...");
        put(1, "rating");
        put(2, "abv");
        put(3, "alphabetically");
    }};

    @Autowired
    public BeerController(BeersService beersService) {
        this.beersService = beersService;
    }

    @GetMapping("/beers-list")
    public String listBeers(Model model,
                            @RequestParam(name = "sortOrder", required = false, defaultValue = "0") Integer sortOrder,
                            @RequestParam(name = "filterStyle", required = false, defaultValue = "0") Integer styleFilter,
                            @RequestParam(name = "filterCountry", required = false, defaultValue = "0") Integer countryFilter,
                            @RequestParam(name = "tagFilter", required = false, defaultValue = "") String tagFilter) {


        List<Beer> beers = beersService.filteredBeers(styleFilter, countryFilter, tagFilter);

        List<BeerListDTO> beersDto;

        switch (sortOrder) {
            case 1:
                beersDto = BeerModelMapper.sortedBeersByRating(beers);
                break;
            case 2:
                beersDto = BeerModelMapper.beerDTOList(beersService.sortByAlcoholVolume(beers));
                break;
            case 3:
                beersDto = BeerModelMapper.beerDTOList(beersService.sortBeerAlphabetically(beers));
                break;
            default:
                beersDto = BeerModelMapper.beerDTOList(beers);
                break;
        }

        model.addAttribute("selectedSorting", sortOrder);
        model.addAttribute("filterByStyle", styleFilter);
        model.addAttribute("filterByCountry", countryFilter);
        model.addAttribute("filterByTag", tagFilter);
        model.addAttribute("sortings", sortings);
        model.addAttribute("styles", beersService.getStyles());
        model.addAttribute("countries", beersService.getCountries());
        model.addAttribute("beers", beersDto);
        model.addAttribute("view", "beer/beers");
        return "base-layout";
    }

    @GetMapping("/beer/{id:\\d+}")
    public String profilePage(Model model, @PathVariable int id) {

        model.addAttribute("beer", BeerModelMapper.toDTO(beersService.getBeerById(id)));
        model.addAttribute("view", "beer/beer-details");

        return "base-layout";
    }

    @GetMapping("/beer/add-beer")
    public String register(Model model) {

        model.addAttribute("styles", beersService.getStyles());
        model.addAttribute("breweries", beersService.getBreweries());
        model.addAttribute("view", "beer/add-beer");

        return "base-layout";
    }

    @PostMapping("/beer/add-beer")
    public String addBeerProcess(BeerAddDTO newBeer) {

        Beer beer = new Beer();

        try {
            beer.setPicture(newBeer.getPicture().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        beer.setLabel(newBeer.getLabel());
        beer.setStyle(beersService.getStyleById(newBeer.getStyleId()));
        beer.setABV(newBeer.getAbv());
        beer.setBrewery(beersService.getBreweryById(newBeer.getBreweryId()));
        beer.setDescription(newBeer.getDescription());

        beersService.addNew(beer);
        return "redirect:/beer-tag/home";
    }

    @GetMapping("/beer/edit-beer/{id:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String editBeer(Model model, @PathVariable int id) {

        model.addAttribute("beer", BeerModelMapper.editBeer(beersService.getBeerById(id)));
        model.addAttribute("styles", beersService.getStyles());
        model.addAttribute("breweries", beersService.getBreweries());
        model.addAttribute("view", "beer/edit-beer");

        return "base-layout";
    }

    @PostMapping("/beer/edit-beer")
//    @PreAuthorize("isAuthenticated()")
    public String editProcess(@ModelAttribute BeerEditDTO beerEdit) {

        this.beersService.editBeer(beerEdit);

        return "redirect:/beer-tag/home/";
    }

    @PostMapping("/beer/delete-beer/{id:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String deleteProcess(@PathVariable int id) {

        this.beersService.deleteBeer(id);

        return "redirect:/beer-tag/beers-list/";
    }

    @GetMapping("/beer/mark-beer/{id:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String markBeer(Model model, @PathVariable int id) {

        model.addAttribute("beer", BeerModelMapper.markBeer(beersService.getBeerById(id)));
        model.addAttribute("users", beersService.getUsers());
        model.addAttribute("marks", beersService.getBeerStatus());
        model.addAttribute("view", "beer/mark-beer");

        return "base-layout";
    }

    @PostMapping("/beer/mark-beer/{beerId:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String markBeerProcess(@PathVariable int beerId, MarkBeerDTO beerMarkDTO) {

        beersService.markBeer(beerMarkDTO.getUser(), beerMarkDTO.getId(), beerMarkDTO.getStatus());

        return "redirect:/beer-tag/beers-list/";
    }

    @GetMapping("/beer/tag-beer/{id:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String tagBeer(Model model, @PathVariable int id) {

        model.addAttribute("beer", BeerModelMapper.tagBeer(beersService.getBeerById(id)));
        model.addAttribute("view", "beer/tag-beer");

        return "base-layout";
    }

    @PostMapping("/beer/tag-beer/{id:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String tagBeerProcess(@PathVariable int id, TagBeerDTO tagBeerDTO) {

        Beer beer = beersService.getBeerById(id);

        beersService.tagBeer(beer.getBeerId(), tagBeerDTO.getTag());

        return "redirect:/beer-tag/beers-list/";
    }

    @GetMapping("/beer/rate-beer/{id:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String rateBeer(Model model, @PathVariable int id) {

        model.addAttribute("beer", BeerModelMapper.tagBeer(beersService.getBeerById(id)));
        model.addAttribute("users", beersService.getUsers());
        model.addAttribute("view", "beer/rate-beer");

        return "base-layout";
    }

    @PostMapping("/beer/rate-beer/{beerId:\\d+}")
//    @PreAuthorize("isAuthenticated()")
    public String rateBeerProcess(@PathVariable int beerId, RateBeerDTO rateBeerDTO) {

        beersService.voteForBeer(rateBeerDTO.getUser(), rateBeerDTO.getId(), rateBeerDTO.getRating());

        return "redirect:/beer-tag/beers-list/";
    }
}
