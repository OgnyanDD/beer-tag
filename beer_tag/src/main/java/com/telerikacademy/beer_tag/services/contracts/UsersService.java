package com.telerikacademy.beer_tag.services.contracts;

import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.user.*;
import com.telerikacademy.beer_tag.models.user.modelsDTO.UserEditDTO;

import java.util.List;

public interface UsersService {

    List<User> listAllUsers();

    User getUserById(int userId);

    void createNewUser(User user);

    void editUserInfo(UserEditDTO dto);

    void editUserRole(int userId, int role);

    UserRole getDefaultRole();

    List<UserRole> getUserRoles();

    List<Beer> getUserFavBeers(int userId);

    void deleteUser(int userId);
}
