package com.telerikacademy.beer_tag.services;

import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.models.user.UserRole;
import com.telerikacademy.beer_tag.models.user.modelsDTO.UserEditDTO;
import com.telerikacademy.beer_tag.repositories.contracts.UsersRepository;
import com.telerikacademy.beer_tag.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    private UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository repository) {
        this.usersRepository = repository;
    }

    @Override
    public List<User> listAllUsers() {
        return usersRepository.listAllUsers();
    }

    @Override
    public User getUserById(int userId) {
        return usersRepository.getById(userId);
    }

    @Override
    public void createNewUser(User user) {
        usersRepository.createNewUser(user);
    }

    @Override
    public void editUserInfo(UserEditDTO dto) {
        usersRepository.editUserInfo(dto);
    }

    @Override
    public void editUserRole(int userId, int role) {
        usersRepository.editUserRole(userId, role);
    }

    @Override
    public UserRole getDefaultRole() {
        return usersRepository.getDefaultRole();
    }

    @Override
    public List<UserRole> getUserRoles() {
        return usersRepository.getUserRoles();
    }

    @Override
    public List<Beer> getUserFavBeers(int userId) {
        return usersRepository.getUserFavBeers(userId);
    }

    @Override
    public void deleteUser(int userId) {
        usersRepository.deleteUser(userId);
    }
}