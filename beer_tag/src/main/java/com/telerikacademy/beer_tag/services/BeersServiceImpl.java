package com.telerikacademy.beer_tag.services;

import com.telerikacademy.beer_tag.models.beer.*;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerEditDTO;
import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.repositories.contracts.BeersRepository;
import com.telerikacademy.beer_tag.services.contracts.BeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service

public class BeersServiceImpl implements BeersService {

    private BeersRepository beersRepository;

    @Autowired
    public BeersServiceImpl(BeersRepository beersRepository) {
        this.beersRepository = beersRepository;
    }

    @Override
    public void addNew(Beer beer) {

        beersRepository.addBeer(beer);
    }

    @Override
    public List<Beer> getAll() {
        return beersRepository.getAll();
    }

    @Override
    public Beer getBeerById(int id) {
        return beersRepository.getBeerById(id);
    }

    @Override
    public void deleteBeer(int id) {
        beersRepository.deleteBeer(id);
    }

    @Override
    public void voteForBeer(int userId, int beerId, int points) {
        beersRepository.voteForBeer(userId, beerId, points);
    }

    @Override
    public void editBeer(BeerEditDTO beerUpdate) {
        beersRepository.editBeer(beerUpdate);
    }

    @Override
    public List<Beer> filterByTag(String tag) {
        return beersRepository.filterBeerByTag(tag);
    }

    @Override
    public List<Beer> filterByCountry(int countryId) {
        return beersRepository.filterBeerByCountry(countryId);
    }

    @Override
    public void tagBeer(int beerId, String tag) {
        beersRepository.tagBeer(beerId,tag);
    }

    @Override
    public List<Beer> sortByAlcoholVolume() {
        return beersRepository.sortByAlcoholVolume();
    }

    @Override
    public List<Beer> sortByAlcoholVolume(List<Beer> beers) {
        return beers.stream().sorted(Comparator.comparingDouble(Beer::getABV)
                .thenComparing(Beer::getLabel)).collect(Collectors.toList());
    }

    @Override
    public List<Beer> sortBeerAlphabetically() {
        return beersRepository.sortBeerAlphabetically();
    }

    @Override
    public List<Beer> sortBeerAlphabetically(List<Beer> beers) {
        return beers.stream().sorted(Comparator.comparing(Beer::getLabel)).collect(Collectors.toList());
    }

    @Override
    public List<Beer> sortBeerByRating() {
        return beersRepository.sortBeerByRating();
    }

    @Override
    public List<Beer> filterBeerByStyle(int styleId) {
        return beersRepository.filterBeerByStyle(styleId);
    }

    @Override
    public List<Style> getStyles() {
        return beersRepository.getStyles();
    }

    @Override
    public List<Brewery> getBreweries() {
        return beersRepository.getBreweries();
    }

    @Override
    public List<Country> getCountries(){
        return beersRepository.getCountries();
    }

    @Override
    public void markBeer(int userId, int beerId, int statusId) {
        beersRepository.markBeer(userId, beerId, statusId);
    }

    @Override
    public List<User> getUsers() {
        return beersRepository.getUsers();
    }

    @Override
    public Style getStyleById(int id) {
        return beersRepository.getStyleById(id);
    }

    @Override
    public Brewery getBreweryById(int id) {
        return beersRepository.getBreweryById(id);
    }

    @Override
    public List<StatusTypes> getBeerStatus() {
        return beersRepository.getBeerStatus();
    }

    @Override
    public List<Beer> filteredBeers(int styleId, int countryId, String tag) {
        return beersRepository.filteredBeers(styleId, countryId, tag);
    }
}
