package com.telerikacademy.beer_tag.services.contracts;

import com.telerikacademy.beer_tag.models.beer.*;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerEditDTO;
import com.telerikacademy.beer_tag.models.user.User;

import java.util.List;

public interface BeersService {

    void addNew(Beer beer);

    List<Beer> getAll();

    Beer getBeerById(int id);

    void deleteBeer(int id);

    void voteForBeer(int userId, int beerId, int points);

    void editBeer(BeerEditDTO beerUpdate);

    List<Beer> sortByAlcoholVolume();

    List<Beer> sortByAlcoholVolume(List<Beer> beers);

    List<Beer> sortBeerAlphabetically();

    List<Beer> sortBeerAlphabetically(List<Beer> beers);

    List<Beer> sortBeerByRating();

    List<Beer> filterBeerByStyle(int styleId);

    List<Beer> filterByTag(String tag);

    List<Beer> filterByCountry(int countryId);

    void tagBeer(int beerId, String tag);

    List<Style> getStyles();

    List<Brewery> getBreweries();

    List<Country> getCountries();

    void markBeer(int userId, int beerId, int statusId);

    List<User> getUsers();

    Style getStyleById(int id);

    Brewery getBreweryById(int id);

    List<StatusTypes> getBeerStatus();

    List<Beer> filteredBeers(int styleId, int countryId, String tag);
}
