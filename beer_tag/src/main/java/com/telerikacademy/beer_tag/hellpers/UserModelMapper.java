package com.telerikacademy.beer_tag.hellpers;

import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.beer.BeerRating;
import com.telerikacademy.beer_tag.models.beer.BeerStatus;
import com.telerikacademy.beer_tag.models.user.*;
import com.telerikacademy.beer_tag.models.user.modelsDTO.*;
import org.apache.tomcat.util.codec.binary.Base64;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserModelMapper {


    public static ChangeUserRoleDTO changeUserRole(User user) {
        return getUserDetails(user);
    }

    private static ChangeUserRoleDTO getUserDetails(User user) {
        ChangeUserRoleDTO result = new ChangeUserRoleDTO();
        result.setId(user.getUserId());
        result.setUserName(user.getUserLogin().getUserLoginName());
        result.setRoleId(user.getUserLogin().getUserRoles().stream().findFirst().get().getUserRoleId());

        return result;
    }

    public static UserProfileDTO setUserProfileDTO(User user) {
        return getUserProfileDetailsDTO(user);
    }

    private static UserProfileDTO getUserProfileDetailsDTO(User user) {
        UserProfileDTO result = new UserProfileDTO();
        result.setId(user.getUserId());
        result.setPicture(new String(Base64.encodeBase64(user.getUserPhoto())));
        result.setFirstName(user.getUserFirstName());
        result.setLastName(user.getUserLastName());
        result.setEmail(user.getUserEmail());
        result.setRole(user.getUserLogin().getUserRoles().stream().findFirst().get().getRole());

        return result;
    }

    public static UserEditDTO editUserProfileDTO(User user) {
        return editUserProfileDetailsDTO(user);
    }

    private static UserEditDTO editUserProfileDetailsDTO(User user) {
        UserEditDTO result = new UserEditDTO();

        result.setUserId(user.getUserId());
        result.setUserPhoto(new String(Base64.encodeBase64(user.getUserPhoto())));
        result.setFirstName(user.getUserFirstName());
        result.setLastName(user.getUserLastName());
        result.setEmail(user.getUserEmail());

        return result;
    }

    public static List<UsersListDTO> setUserListInfoDTO(List<User> userList) {

        List<UsersListDTO> userListInfoDTO = new ArrayList<>();

        for (User user : userList) {
            UsersListDTO result = getUserListDetails(user);
            userListInfoDTO.add(result);
        }
        return userListInfoDTO;
    }

    private static UsersListDTO getUserListDetails(User user) {
        UsersListDTO result = new UsersListDTO();
        result.setId(user.getUserId());
        result.setFirstName(user.getUserFirstName());
        result.setLastName(user.getUserLastName());
        result.setEmail(user.getUserEmail());
        result.setUserName(user.getUserLogin().getUserLoginName());
        result.setRole(user.getUserLogin().getUserRoles().get(0).getRole());

        return result;
    }

    public static List<UserFavBeersDTO> getUserFavBeers(List<Beer> beers, int userId) {

        return beers.stream().map(b -> new UserFavBeersDTO(
                b.getLabel(),
                b.getStyle().getType(),
                b.getABV(),
                b.getBrewery().getBrewery(),
                b.getBrewery().getCountry().getCountry(),
                b.getBeersRatingsList()
                        .stream()
                        .filter(br -> br.getUser().getUserId() == userId)
                        .findFirst().map(BeerRating::getRating).orElse(0),
                getBeerStatusType(b.getBeersStatusesList()
                        .stream()
                        .filter(br -> br.getUser().getUserId() == userId)
                        .findFirst())))
                .collect(Collectors.toList());

    }

    private static String getBeerStatusType(Optional<BeerStatus> beerStatus) {
        if (beerStatus.isPresent()) {
            return beerStatus.get().getStatus().getStatus();
        }

        return "";
    }
}
