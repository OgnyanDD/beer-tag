package com.telerikacademy.beer_tag.hellpers;

import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.beer.BeerRating;
import com.telerikacademy.beer_tag.models.beer.Tag;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.*;
import org.apache.tomcat.util.codec.binary.Base64;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BeerModelMapper {

    public static BeerDetailsDTO toDTO(Beer beer) {
        return getBeerDTO(beer);
    }

    public static List<BeerListDTO> beerDTOList(List<Beer> beerList) {
        List<BeerListDTO> listBeersDTO = new ArrayList<>();
        for (Beer beer : beerList) {
            BeerListDTO result = listBeerDTO(beer);
            listBeersDTO.add(result);
        }
        return listBeersDTO;
    }

    public static List<BeerListDTO> sortedBeersByRating(List<Beer> beerList) {
        List<BeerListDTO> listBeerDTO = new ArrayList<>();
        for (Beer beer : beerList) {
            BeerListDTO result = listBeerDTO(beer);
            listBeerDTO.add(result);
        }
        listBeerDTO = listBeerDTO.stream().sorted(Comparator.comparingDouble(BeerListDTO::getRating).reversed()
                .thenComparing(BeerListDTO::getLabel))
                .collect(Collectors.toList());
        return listBeerDTO;
    }

    private static BeerListDTO listBeerDTO(Beer beer) {
        BeerListDTO result = new BeerListDTO();

        result.setId(beer.getBeerId());
        result.setLabel(beer.getLabel());
        result.setStyle(String.format("%s, %s", beer.getStyle().getType(), beer.getStyle().getCategory()));
        result.setAbv(beer.getABV());
        result.setBrewery(beer.getBrewery().getBrewery());
        result.setCountry(beer.getBrewery().getCountry().getCountry());
        List<BeerRating> ratingList = beer.getBeersRatingsList();
        double rate = getRating(ratingList);
        rate = roundRating(rate);
        result.setRating(rate);
        result.setTags(beer.getTags().stream().map(Tag::getTag).collect(Collectors.joining(" ")));

        return result;
    }

    private static BeerDetailsDTO getBeerDTO(Beer beer) {
        BeerDetailsDTO result = new BeerDetailsDTO();

        result.setId(beer.getBeerId());
        result.setPicture(new String(Base64.encodeBase64(beer.getPicture())));
        result.setLabel(beer.getLabel());
        result.setStyle(beer.getStyle().getType());
        List<BeerRating> ratingList = beer.getBeersRatingsList();

        double rate = getRating(ratingList);
        rate = roundRating(rate);
        result.setRating(rate);
        result.setAbv(beer.getABV());
        result.setBrewery(beer.getBrewery().getBrewery());
        result.setCountry(beer.getBrewery().getCountry().getCountry());
        result.setDescription(beer.getDescription());
        result.setTag(beer.getTags().stream().map(Tag::getTag).collect(Collectors.joining(" ")));

        return result;
    }

    public static List<BeerIndexPageDTO> indexPageDTO(List<Beer> beerList) {
        List<BeerIndexPageDTO> beers = new ArrayList<>();
        for (Beer beer : beerList) {
            BeerIndexPageDTO result = setIndexPageBeer(beer);
            beers.add(result);
        }
        return beers;
    }

    private static BeerIndexPageDTO setIndexPageBeer(Beer beer) {

        BeerIndexPageDTO result = new BeerIndexPageDTO();
        result.setId(beer.getBeerId());
        result.setPicture(new String(Base64.encodeBase64(beer.getPicture())));
        result.setLabel(beer.getLabel());
        return result;
    }

    public static BeerEditDTO editBeer(Beer beer) {
        return editBeerDetailsDTO(beer);
    }

    private static BeerEditDTO editBeerDetailsDTO(Beer beer) {
        BeerEditDTO result = new BeerEditDTO();

        result.setId(beer.getBeerId());
        result.setBeerPhoto(new String(Base64.encodeBase64(beer.getPicture())));
        result.setLabel(beer.getLabel());
        result.setStyleId(beer.getStyle().getId());
        result.setAbv(beer.getABV());
        result.setBreweryId(beer.getBrewery().getBreweryId());
        result.setDescription(beer.getDescription());

        return result;
    }

    private static double getRating(List<BeerRating> ratingList) {
        double rating = 0;
        for (BeerRating r : ratingList) {
            rating += r.getRating();
        }
        return (ratingList.size() / (rating * 1.0));
    }

    public static double roundRating(Double rate) {

        rate *= 100;
        rate = (double) Math.round(rate);
        return rate /= 100;
    }

    public static MarkBeerDTO markBeer(Beer beer) {
        return markBeerDTO(beer);
    }

    private static MarkBeerDTO markBeerDTO(Beer beer) {
        MarkBeerDTO result = new MarkBeerDTO();

        result.setId(beer.getBeerId());
        result.setLabel(beer.getLabel());

        return result;
    }

    public static TagBeerDTO tagBeer(Beer beer) {
        return tagBeerDTO(beer);
    }

    private static TagBeerDTO tagBeerDTO(Beer beer) {
        TagBeerDTO result = new TagBeerDTO();

        result.setId(beer.getBeerId());
        result.setLabel(beer.getLabel());

        return result;
    }
}
