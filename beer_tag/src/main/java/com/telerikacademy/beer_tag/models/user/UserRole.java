package com.telerikacademy.beer_tag.models.user;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "roles")
public class UserRole {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userRoleId;

    @NotNull
    @Column(name = "role")
    @Pattern(regexp = "[A-Za-z]*",
            message = "The user role should contain only letters!")
    @Size(
            min = 2,
            max = 20,
            message = "The user role length must be min {min} characters and max {max} characters!")
    private String role;

    @ManyToMany(mappedBy = "userRoles")
    private List<UserLogin> loginList = new ArrayList<>();

    public UserRole() {
    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}