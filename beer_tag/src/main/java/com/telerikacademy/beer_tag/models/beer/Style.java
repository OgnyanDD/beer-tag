package com.telerikacademy.beer_tag.models.beer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "styles")
public class Style {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "type", length = 45)
    private String type;

    @Column(name = "category", length = 45)
    private String category;

    @JsonIgnore
    @OneToMany(mappedBy = "style")
    private List<Beer> beersStylesList;

    public Style() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Beer> getBeersStylesList() {
        return beersStylesList;
    }

    public void setBeersStylesList(List<Beer> beersStylesList) {
        this.beersStylesList = beersStylesList;
    }
}
