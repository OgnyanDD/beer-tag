package com.telerikacademy.beer_tag.models.beer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "countries")
public class Country {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "country", length = 45, nullable = false)
    private String country;

    @JsonIgnore
    @OneToMany(mappedBy = "country")
    private List<Brewery> breweriesList;

    public Country() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Brewery> getBreweriesList() {
        return breweriesList;
    }

    public void setBreweriesList(List<Brewery> breweriesList) {
        this.breweriesList = breweriesList;
    }
}
