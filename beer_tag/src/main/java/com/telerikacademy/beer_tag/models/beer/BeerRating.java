package com.telerikacademy.beer_tag.models.beer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.beer_tag.models.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "beers_ratings")
public class BeerRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private User user;


    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "beer_id")
    private Beer beer;

    @NotNull
    @Column(name = "rating")
    private int rating;

    public BeerRating() {}

    public BeerRating(User user, Beer beer,int rating) {
        this.user = user;
        this.beer = beer;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
