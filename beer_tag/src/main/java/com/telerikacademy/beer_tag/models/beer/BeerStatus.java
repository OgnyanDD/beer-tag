package com.telerikacademy.beer_tag.models.beer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.beer_tag.models.user.User;

import javax.persistence.*;

@Entity
@Table(name = "beers_status")
public class BeerStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "beer_id")
    private Beer beer;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_type_id")
    private StatusTypes status;

    public BeerStatus() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public StatusTypes getStatus() {
        return status;
    }

    public void setStatus(StatusTypes status) {
        this.status = status;
    }
}