package com.telerikacademy.beer_tag.models.beer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "status_types")
public class StatusTypes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "status", length = 45, nullable = false)
    private String status;

    @JsonIgnore
    @OneToMany(mappedBy = "status")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BeerStatus> beerStatusList;

    public StatusTypes() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BeerStatus> getBeerStatusList() {
        return beerStatusList;
    }

    public void setBeerStatusList(List<BeerStatus> beerStatusList) {
        this.beerStatusList = beerStatusList;
    }
}
