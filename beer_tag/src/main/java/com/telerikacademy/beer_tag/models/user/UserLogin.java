package com.telerikacademy.beer_tag.models.user;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "login")
public class UserLogin {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userLoginId;

    @NotNull
    @Column(name = "user_name")
    @Pattern(regexp = "^[a-zA-Z0-9_.\\-]*$",
            message = "The user name can only contains letters, numbers and special characters like ('-', '_', '.')!")
    @Size(
            min = 5,
            max = 32,
            message = "The user name length must be min {min} characters and max {max} characters!"
    )
    private String userLoginName;

    @NotNull
    @Column(name = "user_password")
    @Pattern(regexp = "^[a-zA-Z0-9_.\\-!@#$]*$",
            message = "The user password can only contains letters," +
                    "numbers and special characters like ('-', '_', '.', '!', '@', '#', '$')!")
    @Size(
            min = 8,
            max = 128,
            message = "The user password length must be min {min} characters and max {max} characters!"
    )
    private String userPassword;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "login_roles", joinColumns = @JoinColumn(name = "login_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<UserRole> userRoles = new ArrayList<>();

    public UserLogin() {
        // public constructor is needed for parsing from/to JSON to work
    }

    public int getUserLoginId() {
        return this.userLoginId;
    }

    public void setUserLoginId(int userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getUserLoginName() {
        return this.userLoginName;
    }

    public void setUserLoginName(String userLogName) {
        this.userLoginName = userLogName;
    }

    public String getUserPassword() {
        return this.userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }
}
