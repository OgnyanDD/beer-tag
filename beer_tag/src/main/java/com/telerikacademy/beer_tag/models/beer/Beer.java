package com.telerikacademy.beer_tag.models.beer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int beerId;

    @NotNull
    @Column(name = "picture")
    private byte[] picture;

    @NotNull
    @Column(name = "label")
    @Size(min = 2, max = 45, message = "Label must be between {min} and {max} characters")
    private String label;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "style_id")
    private Style style;

    @NotNull
    @DecimalMin("0.0")
    @Column(name = "abv")
    private double ABV;

    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @NotNull
    @Column(name = "description")
    @Size(min = 20)
    private String description;

    @NotNull
    @Column(name = "is_deleted")
    private boolean isDeleted;

    @JsonIgnore
    @OneToMany(mappedBy = "beer")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BeerRating> beersRatingsList;

    @JsonIgnore
    @OneToMany(mappedBy = "beer")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BeerStatus> beersStatusesList;

    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(
            name = "beers_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tags = new ArrayList<>();

    public Beer() {
    }

    public Beer(int id, String label, int rating, double abv, String country) {

    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public double getABV() {
        return ABV;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public List<BeerRating> getBeersRatingsList() {
        return beersRatingsList;
    }

    public void setBeersRatingsList(List<BeerRating> beersRatingsList) {
        this.beersRatingsList = beersRatingsList;
    }

    public List<BeerStatus> getBeersStatusesList() {
        return beersStatusesList;
    }

    public void setBeersStatusesList(List<BeerStatus> beersStatusesList) {
        this.beersStatusesList = beersStatusesList;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
