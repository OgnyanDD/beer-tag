package com.telerikacademy.beer_tag.models.user;

import com.telerikacademy.beer_tag.models.beer.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;


@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int userId;

    @NotNull
    @Column (name = "profile_picture")
    private byte[] userPhoto;

    @NotNull
    @Column (name = "first_name")
    @Pattern(regexp = "[A-Za-z]*",
            message = "The first name should contain only letters!")
    @Size(
            min = 2,
            max = 15,
            message = "The first name length must be min {min} characters and max {max} characters!")
    private String userFirstName;

    @NotNull
    @Column (name = "last_name")
    @Pattern(regexp = "[A-Za-z]*",
            message = "The last name should contain only letters!")
    @Size(
            min = 5,
            max = 20,
            message = "The last name length must be min {min} characters and max {max} characters!")
    private String userLastName;

    @NotNull
    @Column (name = "email")
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.(?:[a-zA-Z]{2,6})$", message = "Please provide a valid email address")
    @Size(
            min = 3,
            max = 255,
            message = "The email address length must be min {min} characters and max {max} characters!")
    private String userEmail;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "login_id", referencedColumnName = "id")
    private UserLogin userLogin;

    @NotNull
    @Column(name = "is_deleted")
    private boolean isDeleted;

    @OneToMany (mappedBy = "user")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BeerRating> beersRatingsList;

    @OneToMany (mappedBy = "user")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BeerStatus> beersStatusesList;

    public User() {
        // public constructor is needed for parsing from/to JSON to work
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public byte[] getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(byte[] userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public List<BeerRating> getBeersRatingsList() {
        return beersRatingsList;
    }

    public void setBeersRatingsList(List<BeerRating> beersRatingsList) {
        this.beersRatingsList = beersRatingsList;
    }

    public List<BeerStatus> getBeersStatusesList() {
        return beersStatusesList;
    }

    public void setBeersStatusesList(List<BeerStatus> beersStatusesList) {
        this.beersStatusesList = beersStatusesList;
    }
}