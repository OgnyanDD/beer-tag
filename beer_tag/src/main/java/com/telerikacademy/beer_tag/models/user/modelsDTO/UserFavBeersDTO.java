package com.telerikacademy.beer_tag.models.user.modelsDTO;

public class UserFavBeersDTO {

    private String beerLabel;

    private String beerStyle;

    private double abv;

    private String brewery;

    private String country;

    private int rating;

    private String mark;

    public UserFavBeersDTO() {
    }

    public UserFavBeersDTO(String beerLabel, String beerStyle, double abv, String brewery, String country, int rating, String mark) {
        this.beerLabel = beerLabel;
        this.beerStyle = beerStyle;
        this.abv = abv;
        this.brewery = brewery;
        this.country = country;
        this.rating = rating;
        this.mark = mark;
    }

    public String getBeerLabel() {
        return beerLabel;
    }

    public void setBeerLabel(String beerLabel) {
        this.beerLabel = beerLabel;
    }

    public String getBeerStyle() {
        return beerStyle;
    }

    public void setBeerStyle(String beerStyle) {
        this.beerStyle = beerStyle;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}