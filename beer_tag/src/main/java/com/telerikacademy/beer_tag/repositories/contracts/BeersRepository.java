package com.telerikacademy.beer_tag.repositories.contracts;

import com.telerikacademy.beer_tag.models.beer.*;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerEditDTO;
import com.telerikacademy.beer_tag.models.user.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeersRepository {

    Beer getBeerById(int id);

    List<Beer> getAll();

    void addBeer(Beer beer);

    void deleteBeer(int id);

    void editBeer(BeerEditDTO beerUpdate);

    List<Beer> sortByAlcoholVolume();

    List<Beer> sortBeerAlphabetically();

    List<Beer> sortBeerByRating();

    List<Beer> filterBeerByTag(String tag);

    List<Beer> filterBeerByCountry(int CountryId);

    List<Beer> filterBeerByStyle(int StyleId);

    void voteForBeer(int userId, int beerId, int points);

    Tag createTag(String tag);

    void tagBeer(int beerId, String tag);

    List<Style> getStyles();

    List<Brewery> getBreweries();

    List<Country> getCountries();

    void markBeer(int userId, int beerId, int statusId);

    List<User> getUsers();

    Style getStyleById(int id);

    Brewery getBreweryById(int id);

    List<StatusTypes> getBeerStatus();

    List filteredBeers(int styleId, int countryId, String tag);
}
