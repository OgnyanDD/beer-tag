package com.telerikacademy.beer_tag.repositories;

import com.telerikacademy.beer_tag.exceptions.DatabaseItemNotFoundException;
import com.telerikacademy.beer_tag.exceptions.ExistTagException;
import com.telerikacademy.beer_tag.models.beer.*;
import com.telerikacademy.beer_tag.models.beer.modelsDTO.BeerEditDTO;
import com.telerikacademy.beer_tag.models.user.User;
import com.telerikacademy.beer_tag.repositories.contracts.BeersRepository;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;

import java.util.List;


@Repository
public class SqlBeerRepository implements BeersRepository {


    private SessionFactory sessionFactory;

    @Autowired
    public SqlBeerRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAll() {
        List<Beer> beerList = new ArrayList<>();
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String hql = "FROM Beer WHERE isDeleted = false";
            Query query = session.createQuery(hql);
            beerList = query.list();
            session.close();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return beerList;
    }

    @Override
    public Beer getBeerById(int id) {
        Beer beer = null;
        try (Session session = sessionFactory.openSession()) {

            return beer = session.get(Beer.class, id);

        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void addBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(beer);
            transaction.commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }


    @Override
    public void deleteBeer(int id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Beer beer1 = this.getBeer(id, session);
            String hql = "UPDATE Beer Set isDeleted = TRUE where id = :id";
            session.createQuery(hql)
                    .setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void editBeer(BeerEditDTO beerUpdate) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            Beer beer = getBeer(beerUpdate.getId(), session);

            if (!beerUpdate.getPicture().isEmpty()) {
                try {
                    beer.setPicture(beerUpdate.getPicture().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            beer.setLabel(beerUpdate.getLabel());
            beer.setStyle(getStyleById(beerUpdate.getStyleId()));
            beer.setBrewery(getBreweryById(beerUpdate.getBreweryId()));
            beer.setABV(beerUpdate.getAbv());
            beer.setDescription(beerUpdate.getDescription());

            transaction.commit();

        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public List<Beer> sortByAlcoholVolume() {
        List<Beer> beerList = new ArrayList<>();
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String hgl = "FROM  Beer WHERE  isDeleted = false ORDER BY ABV";
            Query query = session.createQuery(hgl);
            beerList = query.list();
            transaction.commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return beerList;
    }

    @Override
    public List<Beer> sortBeerAlphabetically() {
        List<Beer> beerList = new ArrayList<>();
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String hql = " FROM Beer WHERE isDeleted = false ORDER BY label";
            Query query = session.createQuery(hql);
            beerList = query.list();
            transaction.commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return beerList;
    }

    @Override
    public List<Beer> sortBeerByRating() {

        List<Beer> beerList = getAll();
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String hql = "FROM Beer WHERE isDeleted = false";
            Query query = session.createQuery(hql);
            beerList = query.list();
            transaction.commit();

        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return beerList;
    }

    @Override
    public List<Beer> filterBeerByTag(String tag) {
        List<Beer> beerList;
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String hql = "SELECT t.beersList FROM Tag t WHERE t.tag=:tag";
            Query query = session.createQuery(hql);
            query.setParameter("tag", "#" + tag);
            beerList = query.list();
            transaction.commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return beerList;
    }

    @Override
    public List<Beer> filterBeerByCountry(int countryId) {
        List<Beer> beerList;
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String hql = "FROM Beer b WHERE b.brewery.country.id= :countryId";
            Query query = session.createQuery(hql);
            query.setParameter("countryId", countryId);
            beerList = query.list();
            transaction.commit();

        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return beerList;
    }

    @Override
    public List<Beer> filterBeerByStyle(int styleId) {
        List<Beer> beerList;
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String hql = "FROM Beer b WHERE style.id=: styleId";
            Query query = session.createQuery(hql);
            query.setParameter("styleId", styleId);
            beerList = query.list();
            transaction.commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return beerList;
    }

    @Override
    public void voteForBeer(int userId, int beerId, int points) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            User user = session.get(User.class, userId);
            Beer beer = getBeerById(beerId);

            BeerRating rating = new BeerRating(user, beer, points);
            session.save(rating);
            transaction.commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public Tag createTag(String tag) {

        List<Tag> tagList = new ArrayList<>();
        Tag newTag = null;
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            String hql = "FROM Tag t WHERE t.tag =:tag";
            Query<Tag> query = session.createQuery(hql);
            query.setParameter("tag", tag);
            tagList = query.list();
            if (tagList.size() == 0) {
                newTag = new Tag(tag);
                session.save(newTag);
            } else {
                newTag = tagList.get(0);
            }
            transaction.commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return newTag;
    }

    @Override
    public void tagBeer(int beerId, String tag) throws ExistTagException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Tag newTag = createTag(tag);
            Beer beer = getBeerById(beerId);
            List<Tag> tagList = beer.getTags();
            if (tagList.contains(newTag)) {
                throw new ExistTagException(tag);
            } else {
                tagList.add(newTag);
            }
            int idTag = tagList.stream().filter(t -> t.getTag().equals(tag)).findFirst().get().getId();

            String tagQuery = "INSERT INTO beers_tags (beer_id, tag_id) VALUES (:beerId, :tagId)";

            session.createSQLQuery(tagQuery)
                    .setParameter("beerId", beer.getBeerId())
                    .setParameter("tagId", idTag)
                    .executeUpdate();

            transaction.commit();
            session.close();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Style> getStyles() {
        try (Session session = sessionFactory.openSession()) {

            Query query = session.createQuery("FROM Style");
            return query.list();
        }
    }

    @Override
    public Style getStyleById(int id) {
        try (Session session = sessionFactory.openSession()) {

            String hql = "FROM Style WHERE id = :id";

            Style style = (Style) session.createQuery(hql)
                    .setParameter("id", id)
                    .getSingleResult();

            if (style == null) {
                throw new DatabaseItemNotFoundException(id);
            }
            return style;
        }
    }

    @Override
    public List<Brewery> getBreweries() {
        try (Session session = sessionFactory.openSession()) {

            Query query = session.createQuery("FROM Brewery");
            return query.list();
        }
    }

    @Override
    public Brewery getBreweryById(int id) {
        try (Session session = sessionFactory.openSession()) {

            String hql = "FROM Brewery WHERE id = :id";

            Brewery brewery = (Brewery) session.createQuery(hql)
                    .setParameter("id", id)
                    .getSingleResult();

            if (brewery == null) {
                throw new DatabaseItemNotFoundException(id);
            }
            return brewery;
        }
    }

    @Override
    public List<Country> getCountries() {
        try (Session session = sessionFactory.openSession()) {

            Query query = session.createQuery("FROM Country");
            return query.list();
        }
    }

    @Override
    public void markBeer(int userId, int beerId, int statusId) {

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            Beer beer = getBeer(beerId, session);

            String query = "insert into beers_status (user_id, beer_id, status_type_id) values(:userId, :beerId, :status )";

            session.createSQLQuery(query)
                    .setParameter("userId", userId)
                    .setParameter("beerId", beer.getBeerId())
                    .setParameter("status", statusId)
                    .executeUpdate();

            transaction.commit();
            session.close();
        }
    }

    @Override
    public List<User> getUsers() {
        try (Session session = sessionFactory.openSession()) {

            Query query = session.createQuery("FROM User WHERE is_deleted = 0");
            return query.list();
        }
    }

    @Override
    public List<StatusTypes> getBeerStatus() {
        try (Session session = sessionFactory.openSession()) {

            Query query = session.createQuery("FROM StatusTypes");
            return query.list();
        }
    }

    @Override
    public List<Beer> filteredBeers(int styleId, int countryId, String tag) {
        try (Session session = sessionFactory.openSession()) {

            String hql;
            List beers = new ArrayList<>();

            if (styleId > 0 && countryId > 0 && tag != null && !tag.isEmpty()) {
                hql = "FROM Beer AS b WHERE b.isDeleted=false AND b.style.id = :styleId " +
                        "AND b.brewery.country.id = :countryId AND EXISTS(FROM b.tags AS t WHERE t.tag LIKE :tag)";

                beers = session.createQuery(hql)
                        .setParameter("styleId", styleId)
                        .setParameter("countryId", countryId)
                        .setParameter("tag", "%" + tag + "%")
                        .list();

            } else if (styleId > 0 && countryId > 0) {
                hql = "FROM Beer AS b WHERE b.isDeleted=false AND b.style.id = :styleId AND b.brewery.country.id = :countryId";

                beers = session.createQuery(hql)
                        .setParameter("styleId", styleId)
                        .setParameter("countryId", countryId)
                        .list();
            } else if (styleId > 0 && tag != null && !tag.isEmpty()) {
                hql = "FROM Beer AS b WHERE b.isDeleted=false AND b.style.id = :styleId AND EXISTS(FROM b.tags AS t WHERE t.tag LIKE :tag)";

                beers = session.createQuery(hql)
                        .setParameter("styleId", styleId)
                        .setParameter("tag", "%" + tag + "%")
                        .list();
            } else if (styleId > 0) {
                hql = "FROM Beer AS b WHERE b.isDeleted=false AND b.style.id = :styleId";

                beers = session.createQuery(hql)
                        .setParameter("styleId", styleId)
                        .list();
            } else if (countryId > 0 && tag != null && !tag.isEmpty()) {
                hql = "FROM Beer AS b WHERE b.isDeleted=false AND b.brewery.country.id = :countryId AND EXISTS(FROM b.tags AS t WHERE t.tag LIKE :tag)";

                beers = session.createQuery(hql)
                        .setParameter("countryId", countryId)
                        .setParameter("tag", "%" + tag + "%")
                        .list();
            } else if (countryId > 0) {
                hql = "FROM Beer AS b WHERE b.isDeleted=false AND b.brewery.country.id = :countryId";

                beers = session.createQuery(hql)
                        .setParameter("countryId", countryId)
                        .list();
            } else if (tag != null && !tag.isEmpty()) {
                hql = "FROM Beer AS b WHERE b.isDeleted=false AND EXISTS(FROM b.tags AS t WHERE t.tag LIKE :tag)";

                beers = session.createQuery(hql)
                        .setParameter("tag", "%" + tag + "%")
                        .list();
            } else {
                hql = "FROM Beer AS b WHERE b.isDeleted=false";

                beers = session.createQuery(hql)
                        .list();
            }
            return beers;
        }
    }

    private Beer getBeer(int id, Session session) {
        String hql = "FROM Beer WHERE id = :id and isDeleted=false";

        Beer beer = (Beer) session.createQuery(hql)
                .setParameter("id", id)
                .getSingleResult();

        if (beer == null) {
            throw new DatabaseItemNotFoundException(id);
        }
        return beer;
    }
}
