package com.telerikacademy.beer_tag.repositories;

import com.telerikacademy.beer_tag.exceptions.DatabaseItemNotFoundException;
import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.beer.BeerRating;
import com.telerikacademy.beer_tag.models.user.*;
import com.telerikacademy.beer_tag.models.user.modelsDTO.UserEditDTO;
import com.telerikacademy.beer_tag.repositories.contracts.UsersRepository;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class SqlUserRepository implements UsersRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<User> listAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("FROM User WHERE is_deleted = 0");
            return query.list();
        }
    }

    @Override
    public User getById(int userId) {

        try (Session session = sessionFactory.openSession()) {
            return getUser(userId, session);
        }
    }

    @Override
    public void createNewUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            session.close();
        }
    }

    @Override
    public void editUserInfo(UserEditDTO dto) {

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            User user = getUser(dto.getUserId(), session);

            if (!dto.getPicture().isEmpty()) {
                try {
                    user.setUserPhoto(dto.getPicture().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            user.setUserFirstName(dto.getFirstName());
            user.setUserLastName(dto.getLastName());
            user.setUserEmail(dto.getEmail());

            transaction.commit();
            session.close();
        }
    }

    @Override
    public void editUserRole(int userId, int role) {

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            User user = getUser(userId, session);

            String editRoleQuery = "UPDATE login_roles SET role_id = :role WHERE login_id = :loginId";

            session.createSQLQuery(editRoleQuery).setParameter("role", role)
                    .setParameter("loginId", user.getUserId())
                    .executeUpdate();

            transaction.commit();
            session.close();
        }
    }

    @Override
    public UserRole getDefaultRole() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            return session.get(UserRole.class, 1);
        }
    }

    @Override
    public List<UserRole> getUserRoles() {
        try (Session session = sessionFactory.openSession()) {

            Query query = session.createQuery("FROM UserRole");
            return query.list();
        }
    }

    @Override
    public List<Beer> getUserFavBeers(int userId) {
        try (Session session = sessionFactory.openSession()) {

            User user = getUser(userId, session);

            user.getBeersRatingsList()
                    .sort(Comparator.comparing(BeerRating::getRating)
                            .reversed());

            List<Beer> favBeers = user.getBeersRatingsList().stream()
                    .limit(3).map(BeerRating::getBeer)
                    .collect(Collectors.toList());

            return favBeers;
        }
    }

    @Override
    public void deleteUser(int delUserId) {

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            User user = this.getUser(delUserId, session);

            String userDeleteQuery = "UPDATE User SET is_deleted = 1 WHERE id = :delUserId";
            session.createQuery(userDeleteQuery)
                    .setParameter("delUserId", delUserId)
                    .executeUpdate();

            session.getTransaction().commit();
            session.close();
        }
    }

    private User getUser(int userId, Session session) {

        String selectUserQuery = "FROM User WHERE id = :userId AND is_deleted = 0";
        User user = (User) session.createQuery(selectUserQuery)
                .setParameter("userId", userId)
                .getSingleResult();
        if (user == null) {
            throw new DatabaseItemNotFoundException(userId);
        }
        return user;
    }
}
