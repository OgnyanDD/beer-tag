package com.telerikacademy.beer_tag.repositories.contracts;

import com.telerikacademy.beer_tag.models.beer.Beer;
import com.telerikacademy.beer_tag.models.user.*;
import com.telerikacademy.beer_tag.models.user.modelsDTO.UserEditDTO;

import java.util.List;

public interface UsersRepository {
    List<User> listAllUsers();

    User getById(int userId);

    void createNewUser(User user);

    void editUserInfo(UserEditDTO dto);

    void editUserRole(int userId, int role);

    UserRole getDefaultRole();

    List<UserRole> getUserRoles();

    List<Beer> getUserFavBeers(int userId);

    void deleteUser(int userId);
}