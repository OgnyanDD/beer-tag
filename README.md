# :beers: **Beer Tag Team Project** :beers:

**Preliminary information:**
----

|![](/pic's/Readme_logo1.png)|![](/pic's/Readme_logo2.png)|![](/pic's/Readme_logo3.png)|
|:------------:|:------------:|:------------:|
|[**Telerik Academy**](https://www.telerikacademy.com/)|[**"Team 7" Board in Trello**](https://trello.com/b/ECwa8MK6/beer-tag-team-project)| [***Ognyan Dimitrov***](https://gitlab.com/OgnyanDD)<br> [***Pavel Karaivanov***](https://gitlab.com/Paps1)|


This project was assigned as a group project during a Java programming course at **Telerik Academy**.<br>

**What Is BEER TAG Web Application?**<br>
----
> **BEER TAG** is an web application that allows users to add their favorite beers that they has drunk or would like to drink in the future.
Users also have the ability to edit beer information or delete it.
Any user can sort beer list according to beer ***rating***, ***abv*** and ***alphabet***.
Also beers can be filtered by ***style***, ***origin country*** and by ***tags***.<br>
Any user can mark beer as "***drank***" or "***want to drink***", ***vote for a beer***, or ***put a tag***.<br>
Each beer has detailed information about it from the ABV (alcohol by volume) to the style and description.<br>
In regard to authentication, there are two main roles of users: **USERS** and **ADMINISTRATORS**.<br>
```In the current version of the app, there is no distinction between user roles!```<br>
The future version of the application will provide for the use of the Spring Security,
which will allow the differentiation of user rights and application functionality.<br>
Currently, the app allows you to register a new user, change user profile information, remove a user and change the user's role in the app.

**<details><summary>**Intro Snapshot**</summary> ![IMAGE](/pic's/doc/intro.gif)</details>**
**<details><summary>**Beer UI Snapshots**</summary> ![IMAGE](/pic's/doc/Beer_UI.png)</details>**
**<details><summary>**User UI Snapshots**</summary> ![IMAGE](/pic's/doc/User_UI.png)</details>**

***[Download Demo](/pic's/doc/Beer%20Tag_Demo.mp4)***

**Functionalities**<br>
----
**<details><summary>Beers</summary>  :black_small_square: list all beers<br> :black_small_square: sort beers by rating, abv, label<br> :black_small_square: filter beers by tag, style, origin country<br> :black_small_square: view beer full details<br> :black_small_square: add new beer<br> :black_small_square: edit beer details<br> :black_small_square: delete beer<br> :black_small_square: add tag to beer<br> :black_small_square: rate beer<br>:black_small_square: set beer as "drank" or "want to drink"</details>**
**<details><summary>**Users**</summary> :black_small_square: list all users<br> :black_small_square: register new user<br> :black_small_square: view user profile<br>  :black_small_square: view user favorite beers<br> :black_small_square: edit user profile<br> :black_small_square: delete user<br> :black_small_square: change user role</details>**

**About**<br>
----
1. The backend logic of the app is developed as a REST API, using the Spring MVC framework and Hibernate for relational database access with MySQL.
**<details><summary>**Application Database EER Model Diagram**</summary> ![IMAGE](/pic's/Beer_Tag_EER_Model_Diagram.png)</details>**
**<details><summary>**Application Code Statistic**</summary> ![IMAGE](/pic's/doc/Code_Statistic.png)</details>**
2. For serving the UI, the team had used Thymeleaf, Bootstrap, HTML, CSS and Java Script.
3. Mockito framework is used to test the app -> Total **58 tests** (*39% code coverage*)

**Usage**<br>
----

* Start MySQL server to listen;
* Create data base with name "*beer_tag_database*";<br>
  *You have to use SQL scripts in **[beer_tag_database](/beer_tag_database)** folder*
* Import project to take the dependencies;
* Go to **localhost:8080/beer-tag/** in your web browser to open the application;

